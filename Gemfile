source 'https://rubygems.org'

# BACKEND
gem 'rails', '4.1.6'
gem 'pg'
gem 'mongoid'

# FRONTEND
gem 'sass-rails', '~> 4.0.3'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.0.0'
gem 'jquery-rails'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

group :development, :test do
  gem 'pry-byebug'
end

group :development do
  gem 'capistrano-rails',   '~> 1.1', require: false
  gem 'capistrano-bundler', '~> 1.1', require: false
  gem 'rvm1-capistrano3',             require: false
  gem 'capistrano3-unicorn'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'letter_opener'
  gem 'slackistrano', require: false
end

group :production do
  gem 'unicorn'
end

gem 'ejs'
gem 'haml'
gem 'devise'
gem 'email_verifier'
gem 'jquery-ui-rails'
gem 'squeel'
gem 'american_date'
gem 'select2-rails'
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'pdf-forms'
gem 'settingslogic'
gem 'paperclip', '~> 4.3'
gem 'sitemap_generator'
gem 'groupdate'
gem 'will_paginate'