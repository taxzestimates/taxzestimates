worker_processes 4
user 'deploy', 'deploy'

APP_DEPLOY_DIR = "/data/taxzestimates"
working_directory APP_DEPLOY_DIR + "/current"

listen "#{APP_DEPLOY_DIR}/shared/pids/unicorn.sock", :backlog => 64
listen 8000, :tcp_nopush => true

timeout 180

pid "#{APP_DEPLOY_DIR}/shared/pids/unicorn.pid"

stderr_path "#{APP_DEPLOY_DIR}/shared/log/unicorn.stderr.log"
stdout_path "#{APP_DEPLOY_DIR}/shared/log/unicorn.stdout.log"

preload_app true
GC.respond_to?(:copy_on_write_friendly=) and
  GC.copy_on_write_friendly = true


before_exec do |server|
  ENV['BUNDLE_GEMFILE'] = "#{APP_DEPLOY_DIR}/current/Gemfile"
end

before_fork do |server, worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection
end