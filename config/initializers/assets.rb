# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += [
  'website.css',
  'static_pages.css',
  'funnel.css',
  'user.css',
  'app.css',
  'admin.css',

  'admin.js',
  'signup.js',
  'login.js',
  'website_header.js',
  'website_footer.js',
  'funnel.js',
  'dashboard.js',
  'backbone.js',
  'tax.js'
]
