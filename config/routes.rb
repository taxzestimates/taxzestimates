Rails.application.routes.draw do

  get 'user/new'

  root 'website#home'

  namespace :api do
    resources :computations do
      collection do
        get :latest
      end
      member do
        get :form1040ez
        get :form540ez
      end
      resources :jobs do
        get :w4
        get :stateW4
      end
      resources :spouse do
        resources :jobs do
          get :w4
          get :stateW4
        end
      end
    end

    resources :draft_computations do
      collection do
        post :generate_actual_computation
      end
    end

    resources :invitations do
      collection do
        post :save_invite
      end
    end
  end

  namespace :admin do
    root 'dashboard#index'
    post 'refresh-graph', to: "dashboard#graph_info"

    get 'users' => 'user#index'
    get 'users/export' => 'user#export'
    put 'users/toggle' => 'user#toggle'

    put 'video/toggle' => 'video#toggle'
    put 'video/primary' => 'video#set_primary'

    resources :video do
    end
  end

  devise_for :users, :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => "register", :update => "update", :destroy => "destroy" }, :controllers => { :registrations => 'registrations' }

  devise_scope :user do
    get "login", to: "devise/sessions#new"
    get "logout", to: "devise/sessions#destroy"
    get "register", to: "devise/registrations#new"
    get "forgot-password", to: "devise/passwords#new"
    get "change-password", to: "devise/passwords#edit"
    post "users/avatar", to: "registrations#avatar"
  end

  get 'home' => 'static_pages#index'
  get 'about' => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'privacy-policy' => 'static_pages#privacy_policy'
  get 'terms-and-conditions' => 'static_pages#terms_and_conditions'
  get 'tax' => 'tax#index'
  get 'resources' => 'video#index'

  get 'robots.txt' => 'static_pages#robots'

  #get 'funnel' => 'static_pages#funnel'
end
