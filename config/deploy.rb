# config valid only for current version of Capistrano
lock '3.3.5'

set :stages, %w(beta staging production)
set :default_stage, "production"

set :application, 'taxzestimates'
set :repo_url, 'git@bitbucket.org:taxzestimates/taxzestimates.git'

set :branch, "master"
set :user, "deploy"
set :ssh_options, { forward_agent: true }
set :deploy_to, "/data/#{fetch(:application)}"
set :unicorn_config_path, File.join(fetch(:deploy_to), "current","config", "unicorn.rb")
set :unicorn_pid, File.join(fetch(:deploy_to), "shared","pids", "unicorn.pid")
set :pty, true
set :keep_releases, 3

set :linked_files, %w{config/database.yml config/secrets.yml config/application.yml} # Or anything that you need
set :linked_dirs, %w{bin log tmp pids vendor/bundle public/system} # Or anything that you need

set :slack_webhook, "https://hooks.slack.com/services/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"

set :slack_revision, `git rev-parse origin/post-phase-2-optimization`.strip!
set :slack_title_finished, nil
set :slack_msg_finished, nil
set :slack_fallback_finished, "#{fetch(:slack_deploy_user)} deployed #{fetch(:application)} on #{fetch(:stage)}"
set :slack_fields_finished, [
  {
    "title" => "Project",
    "value" => "TaxBestimates",
    "short" => true
  },
  {
    "title" => "Environment",
    "value" => fetch(:stage),
    "short" => true
  },
  {
    "title" => "Deployer",
    "value" => fetch(:slack_deploy_user),
    "short" => true
  },
  {
    "title" => "Revision",
    "value" => "<https://bitbucket.org/taxzestimates/#{fetch(:application)}/commits/#{fetch(:slack_revision)}|#{fetch(:slack_revision)[0..6]}>",
    "short" => true
  }
]

after 'deploy:publishing', 'deploy:restart'

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web, :app), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  task :restart do
    invoke 'unicorn:legacy_restart'
  end
end
