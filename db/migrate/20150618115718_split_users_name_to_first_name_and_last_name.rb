class SplitUsersNameToFirstNameAndLastName < ActiveRecord::Migration
  def up
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string

    User.all.each do |user|
      unless user.name.blank?
        full_name = user.name.split
        user.update(first_name: full_name[0...-1].join(' '), last_name: full_name.last)
      end
    end

    remove_column :users, :name
  end

  def down
    add_column :users, :name, :string

    User.all.each do |user|
      full_name = "#{user.first_name} #{user.last_name}"
      unless full_name.blank?
        user.update(name: full_name.strip)
      end
    end

    remove_column :users, :first_name
    remove_column :users, :last_name
  end
end
