class AddPreTaxContributions < ActiveRecord::Migration
  def change
    create_table :pre_tax_contributions do |t|
      t.integer :tax_info_id

      t.integer :contribution
      t.decimal :amount, precision: 10, scale: 2

      t.timestamps
    end
  end
end
