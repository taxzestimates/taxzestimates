class AddDeactivationToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :deactivation_reason, :string
  end
end
