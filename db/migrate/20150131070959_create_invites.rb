class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.string :email
      t.string :tax_filing

      t.timestamps
    end
  end
end
