class CreateTaxInfos < ActiveRecord::Migration
  def change
    create_table :tax_infos do |t|
      t.integer :user_id

      # PERSONAL INFO
      t.integer :income_type
      t.integer :filing_status
      t.integer :dependents

      # EARNINGS
      t.decimal :gross_earnings, precision: 10, scale: 2
      t.decimal :pre_tax_deductions, precision: 10, scale: 2

      # TAXES
      t.decimal :social_security, precision: 10, scale: 2
      t.decimal :medicare, precision: 10, scale: 2
      t.decimal :ca_state_tax, precision: 10, scale: 2
      t.decimal :ca_sdi, precision: 10, scale: 2

      t.timestamps
    end
  end
end
