class AddPaystubDateToTaxInfos < ActiveRecord::Migration
  def change
    add_column :tax_infos, :paystub_date, :date
  end
end
