class ChangeVideos < ActiveRecord::Migration
  def change
  	change_column_default :videos, :primary, false
  	change_column_default :videos, :active, true
  end
end
