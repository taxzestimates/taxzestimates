class AddIpAddressToInvites < ActiveRecord::Migration
  def change
    add_column :invites, :ip_address, :string
  end
end
