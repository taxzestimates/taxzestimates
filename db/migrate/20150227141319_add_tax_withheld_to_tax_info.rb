class AddTaxWithheldToTaxInfo < ActiveRecord::Migration
  def change
    add_column :tax_infos, :federal_tax_withheld, :decimal, precision: 10, scale: 2
  end
end
