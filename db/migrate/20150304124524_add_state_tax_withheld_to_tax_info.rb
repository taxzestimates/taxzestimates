class AddStateTaxWithheldToTaxInfo < ActiveRecord::Migration
  def change
    add_column :tax_infos, :state_tax_withheld, :decimal, precision: 10, scale: 2
  end
end
