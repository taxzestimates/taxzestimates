class Emails < ActionMailer::Base
  DEVS_EMAIL = 'devs@taxbestimates.com'
  INFO_EMAIL = 'info@taxbestimates.com'
  MAIL_EMAIL = 'mail@taxbestimates.com'

  layout 'layouts/email_layout'
  default from: MAIL_EMAIL
end
