class InviteNotifier < Emails

  def send_update(invite)
    mail(to: invite.email, subject: 'This is a test email')
  end
end
