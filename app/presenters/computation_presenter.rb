class ComputationPresenter
  attr_reader :data
  delegate :as_json, to: :data

  ANNUALIZED_ATTRIBUTES = [
    :annualized_salary,
    :annualized_federal_taxes_withheld,
    :annualized_state_taxes_withheld,
    :annualized_pre_tax_contributions
  ]

  def initialize(computation)
    if computation.present?
      format_data(computation)
    end
  end

  def format_data(computation)
    @data = computation.attributes
    if computation.finished?
      set_complete_data computation
    end
  end

  def set_complete_data computation
    ANNUALIZED_ATTRIBUTES.each do |attribute|
      if computation.respond_to?(attribute)
        @data[attribute] = computation.send(attribute)
      end
    end

    # complete the data

    if not computation.actual_interest_income.present?
      computation.set_actual_interest_income
    end

    if not computation.joint_interest_income.present?
      computation.set_joint_interest_income
    end

    if not computation.agi.present?
      computation.set_agi
    end

    if not computation.taxable_income.present?
      computation.set_taxable_income
    end

    if not computation.federal_tax_rate.present?
      computation.set_federal_tax_rate
    end

    if not computation.state_tax_rate.present?
      computation.set_state_tax_rate
    end

    if not computation.effective_tax_rate.present?
      computation.set_effective_tax_rate
    end

    if not computation.federal_taxes.present?
      computation.set_federal_taxes
    end

    if not computation.state_taxes.present?
      computation.set_state_taxes
    end

    if not computation.take_home_income.present?
      computation.set_take_home_income
    end

    if not computation.federal_credits.present?
      computation.set_federal_credits
    end

    if not computation.state_credits.present?
      computation.set_state_credits
    end

    if not computation.federal_result.present?
      computation.set_federal_result
    end

    if not computation.state_result.present?
      computation.set_state_result
    end

    if not computation.latest_paycheck.present?
      computation.set_latest_paycheck
    end

    # end data completion

    # if computation is not up to date, recompute

    new_version = BaseComputation::PrimaryComputer.new(computation).get_version

    if computation.version != new_version
      Rails.logger.debug("Computation versions do not match (" + computation.version.to_s + " => " + new_version.to_s + "). UPDATING " + computation._id)

      computation.recompute
    end

    # end recomputing

    @data[:agi] = computation.agi
    @data[:federal_result] = computation.federal_result
    @data[:state_result] = computation.state_result
    @data[:credits] = computation.credits
    @data[:taxable_income] = computation.taxable_income
    #@data[:interest_income] = computation.interest_income
    @data[:actual_interest_income] = computation.actual_interest_income
    @data[:joint_interest_income] = computation.joint_interest_income
    @data[:federal_tax_rate] = computation.federal_tax_rate
    @data[:state_tax_rate] = computation.state_tax_rate
    @data[:effective_tax_rate] = computation.effective_tax_rate
    @data[:federal_taxes] = computation.federal_taxes
    @data[:state_taxes] = computation.state_taxes
    @data[:take_home_income] = computation.take_home_income
    @data[:latest_paycheck] = computation.latest_paycheck
    @data[:jobs] = computation.jobs.map { |j| JobPresenter.new(j) }

    if computation.is_filing_joint?
      @data[:spouse] = SpousePresenter.new(computation.spouse)
    end
  end
end
