class TaxInfoPresenter
  attr_reader :data
  delegate :as_json, to: :data

  ATTRIBUTES = [
    :id,
    :email,
    :name
  ]

  def initialize(tax_info)
    format_data(tax_info)
  end

  def format_data(tax_info)
    @data = {}
    ATTRIBUTES.each do |attribute|
      @data[attribute] = tax_info.send(attribute)
    end
  end
end
