class JobPresenter
  include ActionView::Helpers::NumberHelper

  attr_reader :data
  delegate :as_json, to: :data

  def initialize(job)
    if job.present?
      format_data(job)
    end
  end

  def format_data(job)
    @data = job.attributes
    set_complete_data job
  end

  def set_complete_data job
    # complete the data

    if not job.recommended_federal_withholding.present?
      job.set_recommended_federal_withholding
    end

    if not job.federal_filing_status.present?
      job.set_federal_filing_status
    end

    if not job.federal_allowances.present?
      job.set_federal_allowances
    end

    if not job.recommended_state_withholding.present?
      job.set_recommended_state_withholding
    end

    if not job.state_filing_status.present?
      job.set_state_filing_status
    end

    if not job.state_allowances.present?
      job.set_state_allowances
    end

    # end data completion

    # if computation is not up to date, recompute

    new_version = Job::JobComputer.new(job).get_version

    if job.version != new_version
      Rails.logger.debug("Job versions do not match (" + job.version.to_s + " => " + new_version.to_s + "). UPDATING " + job._id)

      job.recompute
    end

    # end recomputing

    @data[:federal_filing_status] = job.federal_filing_status
    @data[:federal_allowances] = job.federal_allowances
    @data[:federal_withholding] = number_to_currency(job.recommended_federal_withholding, :precision => 2)
    @data[:state_filing_status] = job.state_filing_status
    @data[:state_allowances] = job.state_allowances
    @data[:state_withholding] = number_to_currency(job.recommended_state_withholding, :precision => 2)
    @data[:recommendations] = job.recommendations
    @data[:financial_impacts] = job.financial_impacts
  end
end
