class SpousePresenter
  attr_reader :data
  delegate :as_json, to: :data

  def initialize(spouse)
    if spouse.present?
      format_data(spouse)
    end
  end

  def format_data(spouse)
    @data = spouse.attributes
    set_complete_data spouse
  end

  def set_complete_data spouse
    @data[:jobs] = spouse.jobs.map { |j| JobPresenter.new(j) }
  end
end
