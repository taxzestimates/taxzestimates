json.array!(@invites) do |invite|
  json.extract! invite, :id, :email, :tax_filing
  json.url invite_url(invite, format: :json)
end
