module ApplicationHelper

  BOOTSTRAP_FLASH_MSG = {
    success: 'alert-success',
    error: 'alert-error',
    alert: 'alert-block',
    notice: 'alert-info'
  }

  # HELPERS FOR DEVISE
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def display_messages
    html = ""
    flash.each do |severity, message|
      if message
        if flash[:error].instance_of? ActiveModel::Errors
          message = activerecord_error_list(message)
        end
        severity = "error" if severity == "alert"
        html += content_tag 'div', message, :class => "flash alert #{severity}"
      end
    end
    html
  end

  def devise_error_messages!
    
  end

  # Helper method to display active record model errors (like validation errors)
  def activerecord_error_list(errors)
    error_list = '<ul class="error_list">'
    error_list << errors.collect do |e, m|
      "<li>#{e.humanize unless e == "base"} #{m}</li>"
    end.to_s << '</ul>'
    error_list
  end

  # sortable table headers
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, params.merge(:sort => column, :direction => direction, :page => nil, :per_page => params[:per_page]), {:class => css_class}
  end

  # per page links
  def per_page_link(number)
    css_class = number.to_s == params[:per_page] ? "active" : nil
    link_to number, params.merge(:sort => sort_column, :direction => sort_direction, :page => nil, :per_page => number), {:class => css_class}
  end
end
