class DraftComputation < BaseComputation
  field :current_step, type: Integer

  def generate_actual_computation
    ActualComputation.new(self).generate
  end

end

