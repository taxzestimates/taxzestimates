class Job
  include Mongoid::Document
  include ActionView::Helpers::NumberHelper # ergh, violates MVC. will make do for now

  validates :company_name, :is_currently_employed, :presence => true
  validates :date_of_pay_stub, :paycheck_frequency, :presence => true, :if => :is_current_job?
  validates :salary, :salary_current, :mass_transportation, :mass_transportation_current, :mass_parking_investment, :mass_parking_investment_current, :federal_taxes_withheld_current, :federal_taxes_withheld, :state_taxes_withheld_current, :state_taxes_withheld, :numericality => true

  field :company_name, type: String
  field :is_currently_employed, type: Boolean, default: true

  field :date_of_pay_stub, type: String, default: '1'
  field :paycheck_frequency, type: String, default: 'twice_a_month'

  field :salary, type: BigDecimal, default: 0
  field :salary_current, type: BigDecimal, default: 0
  field :bonus, type: BigDecimal, default: 0

  field :mass_transportation, type: BigDecimal, default: 0
  field :mass_transportation_current, type: BigDecimal, default: 0
  field :mass_parking_investment, type: BigDecimal, default: 0
  field :mass_parking_investment_current, type: BigDecimal, default: 0

  field :federal_taxes_withheld_current, type: BigDecimal, default: 0
  field :state_taxes_withheld_current, type: BigDecimal, default: 0
  field :federal_taxes_withheld, type: BigDecimal, default: 0
  field :state_taxes_withheld, type: BigDecimal, default: 0

  # =====================
  # computed values start
  # =====================

  field :federal_filing_status, type: BigDecimal
  field :state_filing_status, type: BigDecimal
  field :federal_allowances, type: Integer
  field :state_allowances, type: Integer
  field :recommended_federal_withholding, type: BigDecimal
  field :recommended_state_withholding, type: BigDecimal

  # to check for computation revisions and recalculate as needed
  field :version, type: Float, default: 0

  # =====================
  # computed values end
  # =====================

  embedded_in :person, polymorphic: true

  def is_current_job?
    is_currently_employed == true
  end

  def month_frequency
    paycheck_frequency == 'twice_a_month' ? 2 : 1
  end

  def remaining_pay_months
    12 - date_of_pay_stub.to_i
  end

  def remaining_paychecks
    (12 - date_of_pay_stub.to_i) * month_frequency
  end

  def annualized_mass_tranportation
    if is_current_job?
      mass_transportation + (remaining_paychecks * mass_transportation_current)
    else
      mass_transportation
    end
  end

  def annualized_mass_parking_investment
    if is_current_job?
      mass_parking_investment + (remaining_paychecks * mass_parking_investment_current)
    else
      mass_parking_investment
    end
  end

  def annualized_salary
    if is_current_job?
      salary + (remaining_paychecks * salary_current) + bonus - annualized_mass_tranportation - annualized_mass_parking_investment
    else
      salary + bonus - annualized_mass_tranportation - annualized_mass_parking_investment
    end
  end

  def federal_withholdings
    if is_current_job?
      federal_taxes_withheld + (remaining_paychecks * federal_taxes_withheld_current)
    else
      federal_taxes_withheld
    end
  end

  def state_withholdings
    if is_current_job?
      state_taxes_withheld + (remaining_paychecks * state_taxes_withheld_current)
    else
      state_taxes_withheld
    end
  end

  def annualized_pre_tax_contributions
    ComputePreTaxContributions.new(self).compute_annual
  end

  def job_income_percentage
    annualized_salary / person.total_income
  end

  def job_federal_tax_percentage
    federal_taxes / person.total_federal_withholdings
  end

  def job_state_tax_percentage
    state_taxes / person.total_federal_withholdings
  end

  def proposed_federal_withholding_adjustment
    (person.federal_result / remaining_paychecks) * person.income_percentage * job_income_percentage
  end

  def proposed_state_withholding_adjustment
    (person.state_result / remaining_paychecks) * person.income_percentage * job_income_percentage
  end

  def federal_financial_impact
    if recommended_federal_withholding == 0
      federal_taxes_withheld_current * month_frequency
    elsif federal_taxes_withheld_current > recommended_federal_withholding
      (federal_taxes_withheld_current - recommended_federal_withholding) * month_frequency
    elsif federal_taxes_withheld_current < recommended_federal_withholding
      (federal_taxes_withheld_current - recommended_federal_withholding) * month_frequency
    else
      0
    end
  end

  def state_financial_impact
    if recommended_state_withholding == 0
      state_taxes_withheld_current * month_frequency
    elsif state_taxes_withheld_current > recommended_state_withholding
      (state_taxes_withheld_current - recommended_state_withholding) * month_frequency
    elsif state_taxes_withheld_current < recommended_state_withholding
      (state_taxes_withheld_current - recommended_state_withholding) * month_frequency
    else
      0
    end
  end

  def federal_adjustment
    federal_financial_impact * remaining_pay_months
  end

  def state_adjustment
    state_financial_impact * remaining_pay_months
  end

  def current_federal_position
    proposed_federal_withholding_adjustment * remaining_paychecks
  end

  def current_state_position
    proposed_state_withholding_adjustment * remaining_paychecks
  end

  def adjusted_federal_position
    federal_adjustment + current_federal_position
  end

  def adjusted_state_position
    state_adjustment + current_state_position
  end

  # =====================
  # tax options start
  # =====================

  def recommendations
    current = federal_taxes_withheld_current
    to = recommended_federal_withholding
    difference = federal_financial_impact

    current_currency = number_to_currency(current, :precision => 2)
    to_currency = number_to_currency(to, :precision => 2)
    difference_currency = number_to_currency(difference.abs, :precision => 2)

    federal_text =
      if current > to
        "You're overpaying your taxes by <strong>#{difference_currency}</strong> each month. Utilize an updated W-4 and adjust your Federal withholdings from <strong>#{current_currency}</strong> to <strong>#{to_currency}</strong>."
      elsif current < to
        "You're underpaying your taxes by <strong>#{difference_currency}</strong> each month. To avoid fines and penalties, utilize an updated W-4 and adjust your Federal withholdings from <strong>#{current_currency}</strong> to <strong>#{to_currency}</strong>."
      elsif current == 0 and to == 0
        "We're unable to provide you with any tax options since you do not currently have any earnings. Your federal tax " + (person.federal_result > 0 ? "refund" : "liability") + " will be <strong>" + number_to_currency(person.federal_result, :precision => 2).to_s + "</strong>."
      else
        "Congrats, you're in great tax shape. You're optimizing your paycheck and on track to not owe at the end of the year."
      end

    current = state_taxes_withheld_current
    to = recommended_state_withholding
    difference = state_financial_impact

    current_currency = number_to_currency(current, :precision => 2)
    to_currency = number_to_currency(to, :precision => 2)
    difference_currency = number_to_currency(difference.abs, :precision => 2)

    state_text =
      if current > to
        "You're overpaying your taxes by <strong>#{difference_currency}</strong> each month. Utilize an updated W-4 and adjust your State withholdings from <strong>#{current_currency}</strong> to <strong>#{to_currency}</strong>."
      elsif current < to
        "You're underpaying your taxes by <strong>#{difference_currency}</strong> each month. To avoid fines and penalties, utilize an updated W-4 and adjust your State withholdings from <strong>#{current_currency}</strong> to <strong>#{to_currency}</strong>."
      elsif current == 0 and to == 0
        "We're unable to provide you with any tax options since you do not currently have any earnings. Your state tax " + (person.state_result > 0 ? "refund" : "liability") + " will be <strong>" + number_to_currency(person.state_result, :precision => 2).to_s + "</strong>."
      else
        "Congrats, you're in great tax shape. You're optimizing your paycheck and on track to not owe at the end of the year."
      end

    federal = {
      text: federal_text,
      link: { text: 'Federal W-4', url: self.person.class.model_name == 'Spouse' ? "/api/computations/#{self.person.computation.id}/spouse/#{self.person.id}/jobs/#{self.id}/w4" : "/api/computations/#{self.person.id}/jobs/#{self.id}/w4"  }
    }

    state = {
      text: state_text,
      link: { text: 'State W-4', url: self.person.class.model_name == 'Spouse' ? "/api/computations/#{self.person.computation.id}/spouse/#{self.person.id}/jobs/#{self.id}/stateW4" : "/api/computations/#{self.person.id}/jobs/#{self.id}/stateW4"  }
    }

    [federal, state]
  end

  def financial_impacts
    impacts = []

    total_current = federal_taxes_withheld_current + state_taxes_withheld_current
    total_to = recommended_federal_withholding + recommended_state_withholding

    if total_to < total_current
      #amount = number_to_currency((total_current - total_to) * month_frequency, :precision => 2)
      amount = number_to_currency((total_current - total_to) * 2, :precision => 2)
      impacts << "Implement your tax-saving options and increase your take home pay by <strong>#{amount}</strong> each month. Your tax options are updated each month and we suggest that you adjust your withholdings as soon as possible so they are reflected on your next paycheck."
    else
      #amount = number_to_currency((total_to - total_current) * month_frequency, :precision => 2)
      amount = number_to_currency((total_to - total_current) * 2, :precision => 2)
      impacts << "Implement your tax-saving options and increase your monthly tax payments pay by <strong>#{amount}</strong> each month and avoid underpayment fines and penalties. Your tax options are updated each month and we suggest that you adjust your withholdings as soon as possible so they are reflected on your next paycheck."
    end

    impacts
  end

  # =====================
  # tax options end
  # =====================

  # =====================
  # setters start
  # =====================

  def set_federal_filing_status
    update_attribute(:federal_filing_status, Job::JobComputer.new(self).federal_filing_status)
  end

  def set_federal_allowances
    update_attribute(:federal_allowances, Job::JobComputer.new(self).federal_allowances)
  end

  def set_recommended_federal_withholding
    update_attribute(:recommended_federal_withholding, Job::JobComputer.new(self).recommended_federal_withholding)
  end

  def set_state_filing_status
    update_attribute(:state_filing_status, Job::JobComputer.new(self).state_filing_status)
  end

  def set_state_allowances
    update_attribute(:state_allowances, Job::JobComputer.new(self).state_allowances)
  end

  def set_recommended_state_withholding
    update_attribute(:recommended_state_withholding, Job::JobComputer.new(self).recommended_state_withholding)
  end

  def set_version
    update_attribute(:version, Job::JobComputer.new(self).get_version)
  end

  def recompute
    set_federal_filing_status
    set_federal_allowances
    set_recommended_federal_withholding
    set_state_filing_status
    set_state_allowances
    set_recommended_state_withholding
    set_version
  end
end
