# DEPRECATED!! WILL REMOVE THIS!!
class TaxInfo < ActiveRecord::Base
  belongs_to :user
  has_many :pre_tax_contributions, dependent: :destroy

  enum income_type: [ :employee, :contractor ]
  enum filing_status: [
    :single,
    :head_of_household,
    :married_filing_joint,
    :married_filing_separate,
    :surviving_spouse
  ]

  [:gross_earnings, :federal_tax_withheld, :state_tax_withheld].each do |attr_sym|
    define_method "annualized_#{attr_sym}" do
      AnnualizedAmount.new(self).compute(self.send(attr_sym))
    end
  end

  def self.income_type_values
    income_types.keys.map { |w| [w.humanize, w] }
  end

  def self.filing_statuses_values
    filing_statuses.keys.map { |w| [w.humanize, w] }
  end

  def agi
     annualized_gross_earnings - annualized_pre_tax_contributions
  end

  def state_result
    StateResult.new(self).compute
  end

  def federal_result
    FederalResult.new(self).compute
  end

  def annualized_pre_tax_contributions
    ComputePreTaxContributions.new(self).compute_annual.round(2)
  end

  def recommendations
    # CHANGE THIS!
    [ "Adjusting your withholdings from 0 to 3 in order to break even at the end of the year",
      "Contribute $250 from each paycheck to your existing IRA account to reduce your liability from $2000 to $200",
      "Set up and contribute $400 a month to a new IRA account to break even at the end of the year",
      "Set up an Health Savings Account and contribute $43 to increase your refund from $100 to $450",
      "Set up a 529 Plan for your child's education and contribute $300 a month and reduce your liability from $3500 to $500"
    ]
  end

end
