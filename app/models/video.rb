class Video < ActiveRecord::Base
	validates :title, :url, :presence => true

	def deactivate
    update_attribute(:active, false)
  end

  def reactivate
    update_attribute(:active, true)
  end

  def set_primary
  	Video.update_all(:primary => false)
  	update_attribute(:primary, true)
  end

	def self.search(search)
    if search
      where('title LIKE ? OR description LIKE ? or url LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%")
    else
      Video.all
    end
  end
end
