class BaseComputation
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::ActiveRecordBridge
  include Mongoid::Attributes::Dynamic # since we changed stuff around
  include ActionView::Helpers::NumberHelper # ergh, violates MVC. will make do for now

  belongs_to_record :user

  FILING_STATUSES = { single: 'single', married_filing_joint: 'married_filing_joint' }
  FILING_STATUSES_TEXT = [ 'single', 'married_filing_joint' ]

  # first step validation
  validates :first_name, :last_name, :birth_month, :birth_day, :birth_year, :address_1, :city, :state, :zip, :has_health_insurance, :presence => true
  validates :months_without_health_insurance, :numericality => { :greater_than => 0, :only_integer => true }, :if => :has_no_health_insurance?

  # second step validation
  validates :filing_status, :inclusion => { :in => FILING_STATUSES_TEXT }, :if => :step1_complete?

  # third step validation
  #validates :spouse, :presence => true, :if => :step2_complete?, :if => :filing_status == 'married_filing_joint'

  # fourth step validation
  #validate :either_person_has_job, :if => :step3_complete?

  # fifth step validation
  validates :has_interest_income, :has_unemployment_income, :presence => true, :if => :step4_complete?
  validates :interest_income, :numericality => true, :if => :step4_complete?, :if => :has_interest_income?
  validates :is_receiving_unemployment_income, :unemployment_income_received_date, :presence => true, :if => :step4_complete?, :if => :has_unemployment_income?
  validates :unemployment_income, :numericality => true, :if => :has_unemployment_income?
  validates :unemployment_income_bi_weekly, :if => :has_unemployment_income?, :presence => true, :if => :step4_complete?, :if => :is_receiving_unemployment_income?

  field :first_name, type: String
  field :last_name, type: String
  field :mi, type: String

  field :birth_month, type: String
  field :birth_day, type: String
  field :birth_year, type: String

  field :address_1, type: String
  field :address_2, type: String
  field :city, type: String
  field :state, type: String
  field :zip, type: String

  field :social_security_number, type: String
  field :income_type, type: String
  field :filing_status, type: String, default: 'single'

  field :children_dependents, type: Integer
  field :relatives_dependents, type: Integer

  field :number_of_allowances, type: String
  field :retirement_account, type: BigDecimal, default: 0

  field :claim_as_dependent, type: Boolean, default: false
  field :has_health_insurance, type: Boolean, default: false
  field :months_without_health_insurance, type: Integer

  field :has_interest_income, type: Boolean, default: true
  field :interest_income, type: BigDecimal, default: 0
  field :has_unemployment_income, type: Boolean, default: true
  field :is_receiving_unemployment_income, type: Boolean, default: true
  field :unemployment_income_received_date, type: Date, default: Date.today
  field :unemployment_income, type: BigDecimal, default: 0
  field :unemployment_income_bi_weekly, type: BigDecimal, default: 0

  # =====================
  # computed values start
  # =====================

  field :agi, type: BigDecimal
  field :federal_result, type: BigDecimal
  field :state_result, type: BigDecimal
  field :federal_credits, type: String
  field :state_credits, type: String
  field :taxable_income, type: BigDecimal
  field :actual_interest_income, type: BigDecimal
  field :joint_interest_income, type: BigDecimal
  field :federal_tax_rate, type: BigDecimal
  field :state_tax_rate, type: BigDecimal
  field :effective_tax_rate, type: BigDecimal
  field :federal_taxes, type: BigDecimal
  field :state_taxes, type: BigDecimal
  field :take_home_income, type: BigDecimal
  field :latest_paycheck, type: Integer

  # to check for computation revisions and recalculate as needed
  field :version, type: Float, default: 0

  # =====================
  # computed values end
  # =====================

  index({ user_id: 1 })

  embeds_many :dependents
  embeds_many :jobs, as: :person, inverse_of: :person
  embeds_one :spouse, inverse_of: :computation
  accepts_nested_attributes_for :spouse, :jobs
  validates_associated :jobs

  FILING_STATUSES.values.each do |filing_status_str|
    define_method "#{filing_status_str}?" do
      self.filing_status == filing_status_str
    end
  end

  def middle_initial
    mi
  end

  def step1_complete?
    first_name.presence and
    last_name.presence and
    birth_month.presence and
    birth_day.presence and
    birth_year.presence and 
    address_1.presence and
    city.presence and
    state.presence and
    zip.presence
  end

  def step2_complete?
    filing_status.presence
  end

  def step3_complete?
    if is_filing_joint?
      spouse.presence
    else
      true
    end
  end

  def step4_complete?
    jobs.presence
=begin
    date_of_pay_stub.presence and
    paycheck_frequency.presence and
    salary.presence and
    salary_current.presence and
    federal_taxes_withheld_current.presence and
    federal_taxes_withheld.presence and
    state_taxes_withheld_current.presence and
    state_taxes_withheld.presence
=end
  end

  def jobs_count
    jobs.length
  end

  def either_person_has_job?
    total_jobs = 0

    total_jobs += jobs.length

    if is_filing_joint?
      total_jobs += spouse.jobs.length
    end

    total_jobs > 0 ? true : false

    #if total_jobs == 0
    #  errors.add(:jobs, is_filing_joint? ? "Either person must have at least one job" : "You must have at least one job")
    #end
  end

  def has_no_health_insurance?
    has_health_insurance == false    
  end

  def is_filing_joint?
    filing_status == 'married_filing_joint' and spouse
  end

  def has_interest_income?
    has_interest_income == true
  end

  def has_unemployment_income?
    has_unemployment_income == true
  end

  def is_receiving_unemployment_income?
    is_receiving_unemployment_income == true
  end

  def unemployment_income_month
    unemployment_income_received_date.strftime("%m").to_i
  end

  def annualized_unemployment_income
    unemployment_income + ((12 - unemployment_income_month) * (unemployment_income_bi_weekly * 2))
  end

  def actual_unemployment_income
    if has_unemployment_income
      if is_receiving_unemployment_income
        annualized_unemployment_income
      else
        unemployment_income
      end
    else
      0
    end
  end

  def wages_salaries
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += (job.annualized_salary)
      end

      amount -= annualized_pre_tax_contributions
    end

    amount
  end

  def annualized_federal_taxes_withheld
    total = 0

    if jobs.present?
      for job in self.jobs
        total += job.federal_taxes_withheld + (((12 - job.date_of_pay_stub.to_i) * job.federal_taxes_withheld_current) * job.month_frequency)
      end
    end

    total
  end

  def annualized_state_taxes_withheld
    total = 0

    if jobs.present?
      for job in self.jobs
        total += job.state_taxes_withheld + (((12 - job.date_of_pay_stub.to_i) * job.state_taxes_withheld_current) * job.month_frequency)
      end
    end

    total
  end

  def annualized_pre_tax_contributions
    ComputePreTaxContributions.new(self).compute_annual
  end

  def hash_attrs
    Hash[attributes]
  end

  def birth_date
    Date.new(birth_year.to_i, birth_month.to_i, birth_day.to_i)
  end

  def age
    begin
      ((Date.today - birth_date) / 365).to_i
    rescue
      0
    end
  end

  def is_senior?
    age >= 65
  end

  def finished?
    either_person_has_job?
  end

  def home_address
    [address_1, address_2].join(', ')
  end

  def full_name
    [first_name, last_name].join(' ')
  end

  def city_state_zip
    [city, state, zip].join(', ')
  end

  # multiple job totals
  def total_income
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.annualized_salary
      end
    end

    amount
  end

  def total_federal_withholdings
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.federal_withholdings
      end
    end

    amount
  end

  def total_state_withholdings
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.state_withholdings
      end
    end

    amount
  end

  def total_proposed_federal_withholding_adjustment
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.proposed_federal_withholding_adjustment
      end
    end

    amount
  end

  def total_proposed_state_withholding_adjustment
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.proposed_state_withholding_adjustment
      end
    end

    amount
  end

  # joint formulae

  def joint_actual_unemployment_income
    if is_filing_joint?
      actual_unemployment_income + spouse.actual_unemployment_income
    else
      actual_unemployment_income
    end
  end

  def joint_wages_salaries
    if is_filing_joint?
      wages_salaries + spouse.wages_salaries
    else
      wages_salaries
    end
  end

  def joint_agi
    if is_filing_joint?
      agi + spouse.agi
    else
      agi
    end
  end

  def joint_total_federal_withholdings
    if is_filing_joint?
      total_federal_withholdings + spouse.total_federal_withholdings
    else
      total_federal_withholdings
    end
  end

  def joint_total_state_withholdings
    if is_filing_joint?
      total_state_withholdings + spouse.total_state_withholdings
    else
      total_state_withholdings
    end
  end

  def joint_total_income
    if is_filing_joint?
      total_income + spouse.total_income
    else
      total_income
    end
  end

  def income_percentage
    if is_filing_joint?
      total_income / joint_total_income
    else
      1
    end
  end

  def federal_tax_percentage
    if is_filing_joint?
      total_federal_withholdings / joint_total_federal_withholdings
    else
      1
    end
  end

  def state_tax_percentage
    if is_filing_joint?
      total_state_withholdings / joint_total_state_withholdings
    else
      1
    end
  end

  def federal_tax_obligation
    if is_filing_joint?
      joint_total_federal_withholdings * income_percentage
    else
      1
    end
  end

  def state_tax_obligation
    if is_filing_joint?
      joint_total_state_withholdings * income_percentage
    else
      1
    end
  end

  def credits
    {federal: JSON.parse(federal_credits), state: JSON.parse(state_credits)}
  end

  # end joint formulae

  # =====================
  # setters start
  # =====================

  def set_agi
    update_attribute(:agi, BaseComputation::PrimaryComputer.new(self).agi)
  end

  def set_federal_result
    update_attribute(:federal_result, BaseComputation::PrimaryComputer.new(self).federal_result)
  end

  def set_state_result
    update_attribute(:state_result, BaseComputation::PrimaryComputer.new(self).state_result)
  end

  def set_federal_credits
    update_attribute(:federal_credits, BaseComputation::PrimaryComputer.new(self).federal_credits)
  end

  def set_state_credits
    update_attribute(:state_credits, BaseComputation::PrimaryComputer.new(self).state_credits)
  end

  def set_taxable_income
    update_attribute(:taxable_income, BaseComputation::PrimaryComputer.new(self).taxable_income)
  end

  def set_actual_interest_income
    update_attribute(:actual_interest_income, BaseComputation::PrimaryComputer.new(self).actual_interest_income)
  end

  def set_joint_interest_income
    update_attribute(:joint_interest_income, BaseComputation::PrimaryComputer.new(self).joint_interest_income)
  end

  def set_federal_tax_rate
    update_attribute(:federal_tax_rate, BaseComputation::PrimaryComputer.new(self).federal_tax_rate)
  end

  def set_state_tax_rate
    update_attribute(:state_tax_rate, BaseComputation::PrimaryComputer.new(self).state_tax_rate)
  end

  def set_effective_tax_rate
    update_attribute(:effective_tax_rate, BaseComputation::PrimaryComputer.new(self).effective_tax_rate)
  end

  def set_federal_taxes
    update_attribute(:federal_taxes, BaseComputation::PrimaryComputer.new(self).federal_taxes)
  end

  def set_state_taxes
    update_attribute(:state_taxes, BaseComputation::PrimaryComputer.new(self).state_taxes)
  end

  def set_take_home_income
    update_attribute(:take_home_income, BaseComputation::PrimaryComputer.new(self).take_home_income)
  end

  def set_latest_paycheck
    update_attribute(:latest_paycheck, BaseComputation::PrimaryComputer.new(self).latest_paycheck)
  end

  def set_version
    update_attribute(:version, BaseComputation::PrimaryComputer.new(self).get_version)
  end

  def recompute
    set_agi
    set_federal_result
    set_state_result
    set_federal_credits
    set_state_credits
    set_taxable_income
    set_actual_interest_income
    set_joint_interest_income
    set_federal_tax_rate
    set_state_tax_rate
    set_effective_tax_rate
    set_federal_taxes
    set_state_taxes
    set_take_home_income
    set_latest_paycheck
    set_version
  end

  # =====================
  # setters end
  # =====================
end
