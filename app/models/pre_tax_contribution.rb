# THIS IS DEPRECATED AS OF NOW!

class PreTaxContribution < ActiveRecord::Base
  belongs_to :tax_info

  enum contribution: [
    :mass_transit,
    :mass_parking,
    :ira
  ]

  DURATION = {
    MONTHLY: 0,
    YEARLY: 1
  }

  MAX_DEDUCTIONS = {
    mass_transit: {
      amount: 130,
      duration: DURATION[:MONTHLY]
    },

    mass_parking: {
      amount: 250,
      duration: DURATION[:MONTHLY]
    },

    ira: {
      amount: 5_500,
      duration: DURATION[:YEARLY]
    }
  }

  def annualized_amount
    BaseComputation::AnnualizedAmount.new(tax_info).compute(amount)
  end

  def annual_deduction_amount
    if annualized_amount > max_annual_deduction_amount
      max_annual_deduction_amount
    else
      annualized_amount
    end
  end

  def monthly?
    deduction[:duration] == DURATION[:MONTHLY]
  end

  def yearly?
    deduction[:duration] == DURATION[:YEARLY]
  end

  def max_annual_deduction_amount
    amount = deduction[:amount]
    if monthly?
      amount * 12
    else
      if ira?
        max_ira_amount
      else
        amount
      end
    end
  end

  def max_ira_amount
    if user_age < 50
      5_500
    else
      6_500
    end
  end

  def user_age
    50 # TEMPORARY
  end

  def deduction
    MAX_DEDUCTIONS[contribution.to_sym]
  end
end

# class PreTaxContribution
#   include Mongoid::Document

#   field :contribution, type: String
#   field :last_name, type: String
#   field :mi, type: String

#   field :birth_month, type: String
#   field :birth_day, type: String
#   field :birth_year, type: String

#   field :address_1, type: String
#   field :address_2, type: String
#   field :city, type: String
#   field :state, type: String
#   field :zip, type: String

#   field :social_security_number, type: String
#   field :income_type, type: String
#   field :jobs_count, type: Integer

#   embeds_many :dependents
#   embeds_one :spouse
# end
