class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  validates_presence_of :first_name, :last_name
  validates_confirmation_of :password
  has_many_documents :computations
  has_one_document :draft_computation
  has_attached_file :avatar, styles: { medium: "200x200#", thumb: "45x45#" }, default_url: "user/no-photo.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/, :size => { :less_than => 1.megabyte }

  self.per_page = 20

  def full_name
    "#{first_name} #{last_name}".strip
  end

  def account_active?
    deactivated_at.nil?
  end

  def active_for_authentication?
    super && account_active?
  end

  def inactive_message
    account_active? ? super : :locked
  end

  # instead of deleting, indicate the user requested a delete & timestamp it  
  def soft_delete
    update_attribute(:deactivated_at, Time.current)
  end

  def reactivate_account
    update_attribute(:deactivated_at, nil)
  end

  def self.to_csv(options = {})
    attributes = %w{full_name email account_active?}

    CSV.generate(options) do |csv|
      csv << attributes
      all.each do |user|
        csv << attributes.map{ |attr| user.send(attr) }
      end
    end
  end

  def self.search(search)
    if search
      where('first_name LIKE ? OR last_name LIKE ? or email LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%")
    else
      User.all
    end
  end
end
