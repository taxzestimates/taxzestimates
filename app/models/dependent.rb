class Dependent
  include Mongoid::Document

  field :first_name, type: String
  field :last_name, type: String
  field :mi, type: String

  field :birth_month, type: String
  field :birth_day, type: String
  field :birth_year, type: String

  field :social_security_number, type: String

  field :dependent_type, type: String

  embedded_in :computation
end
