class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      record.errors[attribute] << "Please enter valid email."
    end
  end
end

class Invite < ActiveRecord::Base
  validates :email, presence: true, uniqueness: true, case_sensitive: false, email: true
end
