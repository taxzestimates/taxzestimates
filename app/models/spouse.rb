class Spouse
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic # since we changed stuff around

  # third step validation
  validates :first_name, :last_name, :birth_month, :birth_day, :birth_year, :address_1, :city, :state, :zip, :has_health_insurance, :presence => true
  validates :months_without_health_insurance, :numericality => { :greater_than => 0, :only_integer => true }, :if => :has_no_health_insurance?

  # fourth step validation
  #validates :jobs, :presence => true, :if => :step3_complete?

  # fifth step validation
  validates :has_interest_income, :has_unemployment_income, :presence => true, :if => :step4_complete?
  validates :interest_income, :numericality => true, :if => :step4_complete?, :if => :has_interest_income?
  validates :is_receiving_unemployment_income, :unemployment_income_received_date, :presence => true, :if => :step4_complete?, :if => :has_unemployment_income?
  validates :unemployment_income, :numericality => true, :if => :has_unemployment_income?
  validates :unemployment_income_bi_weekly, :if => :has_unemployment_income?, :presence => true, :if => :step4_complete?, :if => :is_receiving_unemployment_income?

  field :first_name, type: String
  field :last_name, type: String
  field :mi, type: String

  field :birth_month, type: String
  field :birth_day, type: String
  field :birth_year, type: String

  field :address_1, type: String
  field :address_2, type: String
  field :city, type: String
  field :state, type: String
  field :zip, type: String

  field :social_security_number, type: String
  field :income_type, type: String

  field :number_of_allowances, type: String
  field :retirement_account, type: BigDecimal, default: 0

  field :claim_as_dependent, type: Boolean, default: false
  field :has_health_insurance, type: Boolean, default: false
  field :months_without_health_insurance, type: Integer

  field :has_interest_income, type: Boolean, default: false
  field :interest_income, type: BigDecimal, default: 0
  field :has_unemployment_income, type: Boolean, default: false
  field :is_receiving_unemployment_income, type: Boolean, default: false
  field :unemployment_income_received_date, type: Date, default: Date.today
  field :unemployment_income, type: BigDecimal, default: 0
  field :unemployment_income_bi_weekly, type: BigDecimal, default: 0

  embedded_in :computation, inverse_of: :spouse
  embeds_many :jobs, as: :person
  accepts_nested_attributes_for :jobs
  validates_associated :jobs

  def step3_complete?
    first_name.presence and
    last_name.presence and
    birth_month.presence and
    birth_day.presence and
    birth_year.presence and 
    address_1.presence and
    city.presence and
    state.presence and
    zip.presence
  end

  def step4_complete?
    jobs.presence
=begin
    date_of_pay_stub.presence and
    paycheck_frequency.presence and
    salary.presence and
    salary_current.presence and
    federal_taxes_withheld_current.presence and
    federal_taxes_withheld.presence and
    state_taxes_withheld_current.presence and
    state_taxes_withheld.presence
=end
  end

  def jobs_count
    jobs.length
  end

  def has_no_health_insurance?
    has_health_insurance == false    
  end

  def has_interest_income?
    has_interest_income == true
  end

  def has_unemployment_income?
    has_unemployment_income == true
  end

  def is_receiving_unemployment_income?
    is_receiving_unemployment_income == true
  end

  def unemployment_income_month
    unemployment_income_received_date.strftime("%m").to_i
  end

  def annualized_unemployment_income
    unemployment_income + ((12 - unemployment_income_month) * (unemployment_income_bi_weekly * 2))
  end

  def actual_unemployment_income
    if has_unemployment_income
      if is_receiving_unemployment_income
        annualized_unemployment_income
      else
        unemployment_income
      end
    else
      0
    end
  end

  def actual_interest_income
    if has_interest_income
      interest_income
    else
      0
    end
  end

  def wages_salaries
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += (job.annualized_salary)
      end

      amount -= annualized_pre_tax_contributions
    end

    amount
  end

  def agi
     wages_salaries + actual_interest_income + actual_unemployment_income
  end

  def state_result
    computation.state_result
  end

  def federal_result
    computation.federal_result
  end

  def annualized_federal_taxes_withheld
    total = 0

    if jobs.present?
      for job in self.jobs
        total += job.federal_taxes_withheld + (((12 - job.date_of_pay_stub.to_i) * job.federal_taxes_withheld_current) * job.month_frequency)
      end
    end

    total
  end

  def annualized_state_taxes_withheld
    total = 0

    if jobs.present?
      for job in self.jobs
        total += job.state_taxes_withheld + (((12 - job.date_of_pay_stub.to_i) * job.state_taxes_withheld_current) * job.month_frequency)
      end
    end

    total
  end

  def annualized_pre_tax_contributions
    BaseComputation::ComputePreTaxContributions.new(self).compute_annual
  end

  def birth_date
    Date.new(birth_year.to_i, birth_month.to_i, birth_day.to_i)
  end

  def age
    begin
      ((Date.today - birth_date) / 365).to_i
    rescue
      0
    end
  end

  def is_senior?
    age >= 65
  end

  def filing_status # need this to handle some stuff for jobs (fed/state filing status)
    'married_filing_joint'
  end

  # multiple job totals
  def total_income
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.annualized_salary
      end
    end

    amount
  end

  def income_percentage
    total_income / computation.joint_total_income
  end

  def federal_tax_percentage
    total_federal_withholdings / computation.joint_federal_taxes
  end

  def state_tax_percentage
    total_state_withholdings / computation.joint_state_taxes
  end

  def federal_tax_obligation
    computation.joint_total_federal_withholdings * income_percentage
  end

  def state_tax_obligation
    computation.joint_total_state_withholdings * income_percentage
  end

  def federal_tax_adjustment
    federal_tax_obligation - total_federal_withholdings
  end

  def state_tax_adjustment
    state_tax_obligation - total_state_withholdings
  end

  def total_federal_withholdings
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.federal_withholdings
      end
    end

    amount
  end

  def total_state_withholdings
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.state_withholdings
      end
    end

    amount
  end

  def total_proposed_federal_withholding_adjustment
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.proposed_federal_withholding_adjustment
      end
    end

    amount
  end

  def total_proposed_state_withholding_adjustment
    amount = 0

    if jobs.present?
      for job in self.jobs
        amount += job.proposed_state_withholding_adjustment
      end
    end

    amount
  end

  def home_address
    [address_1, address_2].join(', ')
  end

  def full_name
    [first_name, last_name].join(' ')
  end

  def city_state_zip
    [city, state, zip].join(', ')
  end
end
