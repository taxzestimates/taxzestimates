class App.Views.Tax.StepResults extends Backbone.View
  templates:
    container: JST['templates/tax/steps/results']
    result: JST['templates/tax/tax_result_box']

  initialize: ->
  	 @model = tz.latestTax

  render: (model) ->
    @model = tz.latestTax
    @setElement(@templates.container(model: @model))
    @renderResults()
    @renderCredits()
    @

  renderResults: ->
    @$('.federal-container').html(@templates.result(
      title: 'Federal',
      amount: parseFloat(@model.get('federal_result')).toFixed(2)
    ))

    @$('.state-container').html(@templates.result(
      title: 'State',
      amount: parseFloat(@model.get('state_result')).toFixed(2)
    ))

  renderCredits: ->
    credits = @model.credits()
    _.each credits.federal, (credit) =>
      @$('#federal-tax-credits-table tbody').append("<tr><td>#{credit.text}</td><td class='amount'>#{credit.amount}</td></tr>")

    _.each credits.state, (credit) =>
      @$('#state-tax-credits-table tbody').append("<tr><td>#{credit.text}</td><td class='amount'>#{credit.amount}</td></tr>")
