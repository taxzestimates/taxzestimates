class App.Views.Tax.ACA extends Backbone.View
  templates:
    container: JST['templates/tax/steps/aca']

  SCENARIOS:
    0: "{0} does not qualify for a health insurance exemption and will be subjected to the Shared Responsibility Fee."
    1: "{0} had health insurance all year and will not be subject to the Shared Responsibility Fee."
    2: "There is an exemption for those who go three consecutive months or less without health insurance. {0} has currently gone three consecutive months or less without insurance and will not be subject to the Shared Responsibility Fee."
    3: "{0}'s projected earnings are below the tax filing requirement threshold and will not be subject to the Shared Responsibility Fee."

  render: (model) ->
    @model = model
    @aca = @getScenario()
    @setElement(@templates.container(model: @model, aca: @aca, scenarios: @SCENARIOS))
    @

  getScenario: ->
    @aca = 
      'self': 0
      'spouse': 0

    #check spouse ACA
    if @model.isFilingJoint() == true
      @filingStatus = "joint"

      if @model.spouse.hasHealthInsurance() in [true, "1"] # weird bug where spouse info saves as 1 instead of true for first load
        @aca.spouse = 1
      else if @model.spouse.monthsWithoutHealthInsurance() <= 3
        @aca.spouse = 2
      else if @model.spouse.agi < (parseInt tz.STANDARD_DEDUCTION[@filingStatus] + parseInt tz.PERSONAL_EXEMPTION[@filingStatus])
        @aca.spouse = 3
    else
      @filingStatus = "single"

    # check user ACA
    if @model.hasHealthInsurance() == true
    	@aca.self = 1
    else if @model.monthsWithoutHealthInsurance() <= 3
    	@aca.self = 2
    else if @model.agi < (parseInt tz.STANDARD_DEDUCTION[@filingStatus] + parseInt tz.PERSONAL_EXEMPTION[@filingStatus])
      @aca.self = 3

    @aca

  save: ->
    @
    