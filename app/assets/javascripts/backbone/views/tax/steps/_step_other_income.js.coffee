class App.Views.Tax.StepOtherIncome extends Backbone.View
  templates:
    joint: JST['templates/tax/steps/other_income_joint']
    single: JST['templates/tax/steps/other_income_single']

  render: (model) ->
    @model = model

    html =
      if @model.isFilingJoint()
        @templates.joint(model: @model, spouse: @model.spouse)
      else
        @templates.single(model:@model)

    @setElement(html)

    setTimeout () ->
      $('input[type=radio][name="has_interest_income"]:checked,
         input[type=radio][name="has_unemployment_income"]:checked,
         input[type=radio][name="self_is_receiving_unemployment_income"]:checked,
         input[type=radio][name="spouse_is_receiving_unemployment_income"]:checked').change()

      $('.dp').datepicker()
    , 0

    @

  save: ->
    form = @$('.user-other-income-form').serializeObject()
    has_self = ['self', 'both']

    if @model.isFilingJoint()
      has_spouse = ['spouse', 'both']

    self =
      has_interest_income:                        if form.has_interest_income in has_self then 1 else 0
      interest_income:                            form.self_interest_income
      has_unemployment_income:                    if form.has_unemployment_income in has_self then 1 else 0

    if form.has_unemployment_income in has_self
      self.is_receiving_unemployment_income       = if form.self_is_receiving_unemployment_income == "1" then 1 else 0
      self.unemployment_income_received_date      = if form.self_unemployment_income_received_date then moment(form.self_unemployment_income_received_date).format('MMMM DD, YYYY') else null
      self.unemployment_income                    = form.self_unemployment_income
      self.unemployment_income_bi_weekly          = if form.self_is_receiving_unemployment_income == "1" then form.self_unemployment_income_bi_weekly else 0

    if @model.isFilingJoint()
      spouse =
        has_interest_income:                        if form.has_interest_income in has_spouse then 1 else 0
        interest_income:                            form.spouse_interest_income
        has_unemployment_income:                    if form.has_unemployment_income in has_spouse then 1 else 0

      if form.has_unemployment_income in has_spouse
        spouse.is_receiving_unemployment_income     = if form.spouse_is_receiving_unemployment_income == "1" then 1 else 0
        spouse.unemployment_income_received_date    = if form.spouse_unemployment_income_received_date then moment(form.spouse_unemployment_income_received_date).format('MMMM DD, YYYY') else null
        spouse.unemployment_income                  = form.spouse_unemployment_income
        spouse.unemployment_income_bi_weekly        = if form.spouse_is_receiving_unemployment_income == "1" then form.spouse_unemployment_income_bi_weekly else 0

      @model.spouse.set(spouse)

    @model.set(self)
