class App.Views.Tax.PersonalInformation extends Backbone.View
  currentStep: 1
  lastStep: 6
  loaded: false

  templates:
    container: JST['templates/tax/personal_information_container']
    filters: JST['templates/tax/personal_information_filters']

  events:
    'click .save-button': 'save'

  views:
    personalInfo: new App.Views.Tax.StepPersonalInfo
    filingStatus: new App.Views.Tax.StepFilingStatus
    # exemptions: new App.Views.Tax.StepExemptions
    dependents: new App.Views.Tax.StepDependents
    incomeInfo: new App.Views.Tax.StepIncomeInfo
    otherIncome: new App.Views.Tax.StepOtherIncome

    aca: new App.Views.Tax.ACA
    results: new App.Views.Tax.StepResults

  initialize: ->
    @user = tz.user

    @model = if !tz.draftTax then new App.Models.DraftComputation else tz.draftTax
    @setupListeners()

  render: ->
    @setElement(@templates.container()) unless @loaded
    @$('.personal-info-filter-container').html(@templates.filters())

    @$('[name="social_security_number"]').mask('000-00-0000');

    @evaluateOptions()
    @checkFilingStatus()
    @

  setupListeners: ->
    @model.on 'finishSave', @finishSave, @

  finishSave: ->
    if @isLastStep()
      @generateComputation()
    else
      @nextStep()

  renderOption: (stepNum) ->
    $option = @$(".options:not(.disabled)[data-step=#{stepNum}]")

    if $option.length > 0

      @$('.options').removeClass('active')
      $option.addClass('active')
      # $option.prev().addClass('beforeActive')
      @currentStep = stepNum
      @renderView($option.data('view'))

    else
      # Always fallback to step 1
      @navigateToStep(1)

  navigateToStep: (step) ->
    App.router.navigate("personal_information/#{step}", trigger: true)

  renderView: (template) ->
    @currentView = @views[template]

    @$('.personal-info-content-container').html(
      @currentView.render(@model).$el
    )

    @currentView.afterShow() if @currentView.afterShow
    @currentView.$('form').validate(ignore: [])

  setStep: (step) ->
    step = step || @model.currentStep() || 1
    step = parseInt step
    @renderOption(step)

  save: ->
    event.preventDefault()

    @currentView.save()

    # Always save draft computation
    newStep = @currentStep + 1

    if newStep > @model.currentStep()
      @model.set(current_step: newStep)


    @model.saveComputation()

  isLastStep: ->
    @currentStep == @lastStep

  nextStep: ->
    nextStepNum = @currentStep + 1
    @navigateToStep(nextStepNum)

  evaluateOptions: ->
    maxStep = parseInt @model.currentStep()
    $disabledOptions =
      @$('.options:not(.active)').filter ->
        $(@).data('step') > maxStep
    $disabledOptions.addClass('disabled').parent().removeAttr('href')

  checkFilingStatus: ->
    if @model.isFilingJoint() == false
      @$("#filter-dependents").addClass('null').parent().removeAttr('href')

  generateComputation: ->
    $.ajax
      url: '/api/draft_computations/generate_actual_computation'
      method: 'POST'
      success: (resp) =>
        tz.latestTax.set(resp)
        tz.latestTax.setDependentAndSpouse()
        tz.latestTax.setJobs()
        tz.latestTax.setSpouseJobs()
        @showResult()

  showResult: ->
    # REDIRECT FOR NOW.
    #window.location = "/tax#dashboard"
    @nextStep()
