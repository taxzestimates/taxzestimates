class App.Views.Tax.StepPersonalInfo extends Backbone.View
  template: JST['templates/tax/steps/personal_info']

  render: (model) ->
    @model = model
    @setElement(@template(model: @model))

    setTimeout () ->
      $('input[type=radio][name="has_health_insurance"]:checked').change()
      $('[data-toggle="popover"]').popover({ html: true, trigger: 'hover' })
    , 0

    @

  afterShow: ->
    @setDropdowns()

  setDropdowns: ->
    @$("input[name=birth_month]").select2(data: tz.utils.monthsDropdownData)
    @$("input[name=birth_year]").select2(data: tz.utils.yearsDropdownData)
    @$("input[name=birth_day]").select2(data: tz.utils.daysDropdownData)
    @$("input[name=state]").select2(data: tz.utils.statesDropdownData)
    @$("input[name=income_type]").select2(data: tz.utils.select2data(@model.INCOME_TYPES))

  save: ->
    @model.set(@$('form').serializeObject(), { silent: true })
