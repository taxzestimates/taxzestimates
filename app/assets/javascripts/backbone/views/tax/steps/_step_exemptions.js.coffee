class App.Views.Tax.StepExemptions extends Backbone.View
  template: JST['templates/tax/steps/exemptions']

  events:
    'change .dependents': 'dependentChange'

  render: (model) ->
    @model = model
    @setElement(@template(model: @model))
    @

  afterShow: ->
    @setDropdowns()

  dependentChange: (event) ->
    children = parseInt @$("input[name=children_dependents]").val()
    relatives = parseInt @$("input[name=relatives_dependents]").val()
    total = children + relatives
    @$('.total-dependents-count').text(total)

  setDropdowns: ->
    data = _.map(_.range(0, 5), (v) ->
      day = v.toString()
      {id: day, text: day}
    )
    @$("input[name=children_dependents]").select2(data: data)
    @$("input[name=relatives_dependents]").select2(data: data)

  save: ->
    @model.set(@$('form').serializeObject())

    # Reset dependents collection
    @model.dependents.reset()
