class App.Views.Tax.StepFilingStatus extends Backbone.View
  template: JST['templates/tax/steps/filing_status']

  INFO_TEXTS:
    single: "The IRS considers taxpayers who were unmarried, legally separated, or divorced as of the last day of the year to be single. The standard deduction for single filers in %year% is $6,300."
    head_of_household: "You're the head of household yeah!"
    married_filing_joint: "The IRS allows taxpayers who were legally married as of the last day of the year to file their tax return as Married Filing Joint. The standard deduction for Married Filing Joint filers in %year% is $12,600."
    married_filing_separate: "You're married but filing separately!"
    surviving_spouse: "You're a surviving spouse!"

  events:
    'change .filing_status': 'filingStatusChanged'

  render: (model) ->
    @model = model
    @setElement(@template(model: @model))
    @

  afterShow: ->
    @setDropdowns()
    @setInfoContent(@model.filingStatus())

  setDropdowns: ->
    @$("input[name=filing_status]").select2(data: tz.utils.select2data(tz.FILING_STATUSES))

  setInfoContent: (filing_status) ->
    if filing_status
      @$('.info-content').html(@INFO_TEXTS[filing_status].replace('%year%', tz.YEAR))

  filingStatusChanged: (event) ->
    @setInfoContent $(event.currentTarget).val()

  save: ->
    @model.set(@$('form').serializeObject())

    if @model.isFilingJoint()
      @model.setSpouseJobs()
    else if !@model.spouse.isNew()
      @model.spouse.clear()
