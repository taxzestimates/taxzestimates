class App.Views.Tax.StepDependents extends Backbone.View

  templates:
    container: JST['templates/tax/steps/dependents']
    spouseForm: JST['templates/tax/steps/spouse_form']
    dependentForm: JST['templates/tax/steps/dependent_form']

  render: (model) ->
    @model = model
    @setElement(@templates.container())

    setTimeout () ->
      $('input[type=radio][name="has_health_insurance"]:checked').change()
      $('[data-toggle="popover"]').popover({ html: true, trigger: 'hover' })
    , 0

    @renderForms()
    @

  afterShow: ->
    if @withSpouseOrDependents()
      @setDropdowns()

    else
      @$('.save-button').click()

  withSpouseOrDependents: ->
    @model.isFilingJoint() || !!@model.dependentsCount()

  renderForms: ->
    @renderSpouseForm()
    @renderDependents()

  renderSpouseForm: ->
    if @model.isFilingJoint()
      @$('.spouse-container .form-container').html(@templates.spouseForm(model: @model.spouse))
      @$('.spouse-container').removeClass('hidden')

  renderDependents: ->
    if @model.dependents.length > 0
      @renderLoadedDependents()
    else
      @renderNewDependents()

  renderLoadedDependents: ->
    @model.dependents.each (dependent) =>
      @renderDependent(dependent)

  renderNewDependents: ->
    childCount = parseInt @model.childrenDependents()
    relativeCount = parseInt @model.relativesDependents()

    childDependent = new App.Models.Dependent(dependent_type: 'child')
    relativeDependent = new App.Models.Dependent(dependent_type: 'relative')

    _(childCount).times (num) => @renderDependent(childDependent)
    _(relativeCount).times (num) => @renderDependent(relativeDependent)

  renderDependent: (dependent) ->
    $container =
      if dependent.isChild()
        @$('.child-dependents-container .form-container')
      else
        @$('.relative-dependents-container .form-container')

    $container.append(@templates.dependentForm(model: dependent))

    if @model.hasRelativeDependent()
      @$('.relative-dependents-container').removeClass('hidden')

    if @model.hasChildDependent()
      @$('.child-dependents-container').removeClass('hidden')


  setDropdowns: ->
    @$("input[name=birth_month]").select2(data: tz.utils.monthsDropdownData)
    @$("input[name=birth_year]").select2(data: tz.utils.yearsDropdownData)
    @$("input[name=birth_day]").select2(data: tz.utils.daysDropdownData)
    @$("input[name=state]").select2(data: tz.utils.statesDropdownData)
    @$("input[name=income_type]").select2(data: tz.utils.select2data(@model.INCOME_TYPES))

  save: ->
    # Set Spouse
    @model.spouse.set(@$('form.spouse-form').serializeObject())

    # Set dependents
    @model.dependents.reset()

    _.each @$('form.dependent-form'), (form) =>
      @model.dependents.add($(form).serializeObject())
