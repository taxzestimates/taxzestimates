class App.Views.Tax.MainView extends Backbone.View
  el: '#tax-main-container'
  template: JST['templates/tax/main_view']
  loaded: false

  initialize: ->
    @subNav = new App.Views.Tax.SubNav
    @dashboard = new App.Views.Tax.Dashboard
    @recommendations = new App.Views.Tax.Recommendations
    @personalInfo = new App.Views.Tax.PersonalInformation
    @archive = new App.Views.Tax.Archive
    @annualFiling = new App.Views.Tax.AnnualFiling

  render: ->
    @$el.html(@template())
    @loaded = true
    @

  renderDashboard: ->
    @setView(@dashboard) if @renderSub('dashboard')


  renderPersonalInformation: (step) ->
    if @renderSub('personal-information')
      @setView(@personalInfo)
      @personalInfo.setStep(step)


  renderRecommendations: ->
    @setView(@recommendations) if @renderSub('recommendations')


  renderArchive: ->
    @setView(@archive) if @renderSub('archive')


  renderAnnualFiling: ->
    @setView(@annualFiling) if @renderSub('annual-filing')


  renderSub: (active = 'dashboard') ->
    @render() unless @loaded
    allowed = !tz.latestTax.isNew() || (active is 'personal-information')
    if allowed
      @$('.sub-nav-container').html(@subNav.render(active).$el)
    else
      App.router.navigate('personal_information', trigger: true)

    allowed

  setView: (view) ->
    @$('.content-container').html(view.render().$el)
    view.afterShow() if view.afterShow

    if tz.draftTax
      banner = tz.draftTax.getBanner()

      if banner
        $('.banner').addClass(banner[0]).find('.container').html(banner[1])
        $('.banner').show()
        $('body').css('padding-top', $('.banner').outerHeight())
      else if $('.banner').is(':visible')
        $('.banner').removeClass('warning notice').hide()
        $('body').css('padding-top', 0)
