class App.Views.Tax.Recommendations extends Backbone.View
  templates:
    container: JST['templates/tax/recommendations']
    result: JST['templates/tax/tax_result_box']
    recommendationTable: JST['templates/tax/recommendations_table']
    financialImpactTable: JST['templates/tax/financial_impact_table']
    recommendationRow: JST['templates/tax/recommendations_table_row']
    financialImpactRow: JST['templates/tax/financial_impact_row']
    jobTab: JST['templates/tax/recommendations_job_tab']
    jobTabContent: JST['templates/tax/recommendations_job_tab_content']
    zpInfo: JST['templates/tax/zp_info']

  initialize: ->
    @model = tz.latestTax

  render: ->
    @totalJobs = @model.jobs.length

    if @model.isFilingJoint()
      @totalJobs += @model.spouse.jobs.length

    if @model.isFilingJoint()
      @showPills = if @model.jobs.length > 1 or @model.spouse.jobs.length > 1 then true else false
    else
      @showPills = if @model.jobs.length > 1 then true else false


    @$el.html(@templates.container(model: @model, showPills: @showPills))
    @renderJobs()
    @renderLatestResults()
    @

  afterShow: ->
    view = @

    @$("#tab-primary-user").on 'show.bs.tab', () ->
      $("#secondary-user").hide().find('li').removeClass('active')
      $("#primary-user").show().find('li:first a').tab('show')
      $("#job-content").find('.tab-pane:not(.spouse-job)').removeClass('in active')

    @$("#tab-secondary-user").on 'show.bs.tab', () ->
      $("#primary-user").hide().find('li').removeClass('active')
      $("#secondary-user").show().find('li:first a').tab('show')
      $("#job-content").find('.tab-pane.spouse-job').removeClass('in active')

  renderJobs: ->
    first = true

    if @model.jobs.length > 0
      @model.jobs.each (job, index) =>
        @renderJobTab(job, false, index, first)
        @renderJobTabContent(job, false, first)
        @renderRecommendationsTable(job)
        @renderFinancialImpactTable(job)
        @renderZpInformation(job)
        first = false

    if @model.isFilingJoint()
      if @model.spouse.jobs.length > 0
        @model.spouse.jobs.each (job, index) =>
          @renderJobTab(job, true, index, first)
          @renderJobTabContent(job, true, first)
          @renderRecommendationsTable(job)
          @renderFinancialImpactTable(job)
          @renderZpInformation(job)
          first = false

  renderJobTab: (job, for_spouse, index, is_first) ->
    block = $.parseHTML(@templates.jobTab(job: job, index: (index + 1), first: is_first))

    $container =
      if for_spouse
        @$('#secondary-user .nav-pills')
      else
        @$('#primary-user .nav-pills')

    $container.append(block)

  renderJobTabContent: (job, for_spouse, is_first) ->
    block = $.parseHTML(@templates.jobTabContent(job: job, for_spouse: for_spouse, first: is_first, ))
    $container = @$('#job-content')
    $container.append(block)

  renderRecommendationsTable: (job) ->
    $container = @$('#job-' + job.getID()).find('.recommendations-box')
    $container.append(@templates.recommendationTable(job: job))

    $table = $container.find('.recommendations-table')

    # Iterate recommendations
    _.each job.recommendations(), (recommendation) =>
      $table.find('tbody').append(@templates.recommendationRow(recommendation: recommendation))

  renderFinancialImpactTable: (job) ->
    $container = @$('#job-' + job.getID()).find('.financial-impact-box')
    $container.append(@templates.financialImpactTable(job: job))

    $table = $container.find('.financial-impact-table')

    # Iterate recommendations
    _.each job.financialImpacts(), (financialImpact) =>
      $table.find('tbody').append(@templates.financialImpactRow(financialImpact: financialImpact))

  renderZpInformation: (job) ->
    $container = @$('#job-' + job.getID()).find('.gusto-box')
    $container.append(@templates.zpInfo(job: job))

    @$("#gusto-period-" + job.getID()).html(job.dateText())

    if job.gustoFedFilingStatus() != 'Exempt'
      @$("#gusto-fed-box-" + job.getID()).find(".non-exempt").removeClass('hide')
      @$("#gusto-fed-allowances-" + job.getID()).html(if job.fedAllowances() then 'Move to ' + job.fedAllowances())
      @$("#gusto-fed-withholding-" + job.getID()).html(job.fedWithholding())
    else
      @$("#gusto-fed-box-" + job.getID()).find(".non-exempt").addClass('hide')
      @$("#gusto-fed-allowances-" + job.getID() + ", #gusto-fed-withholding-" + job.getID()).html("")

    @$("#gusto-fed-filing-status-" + job.getID()).html(job.gustoFedFilingStatus())
    @$("#gusto-state-filing-status-" + job.getID()).html(job.gustoStateFilingStatus())

    if job.stateAllowances()
      @$("#gusto-state-allowances-" + job.getID()).html('Move to ' + job.stateAllowances()).parent().removeClass('hide')
    else
      @$("#gusto-state-allowances-" + job.getID()).parent().addClass('hide')

    @$("#gusto-state-withholding-" + job.getID()).html(job.stateWithholding())

  renderLatestResults: ->
    $container = @$('.latest-results-container')

    # FEDERAL
    $container.append(@templates.result(
      title: 'Federal',
      amount: parseFloat(@model.get('federal_result')).toFixed(2)
    ))

    # STATE
    $container.append(@templates.result(
      title: 'State',
      amount: parseFloat(@model.get('state_result')).toFixed(2)
    ))
