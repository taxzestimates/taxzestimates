class App.Views.Tax.SubNav extends Backbone.View
  template: JST['templates/tax/sub_nav']

  render: (active = 'dashboard') ->
 		@user = tz.user
 		@isAdmin = @user.get 'admin'
 		@setElement(@template(admin: @isAdmin))
 		@$("#sub-#{active}").addClass('active')
 		@
