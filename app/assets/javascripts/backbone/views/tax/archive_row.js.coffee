class App.Views.Tax.ArchiveRow extends Backbone.View
  templates:
    container: JST['templates/tax/archive_row']

  initialize: (options) ->
    @model = options.model
    @monthText = options.monthText
    @hidden = options.hidden

  render: ->
    @setElement(@templates.container(model: @model, monthText: @monthText, hidden: @hidden))
    @
