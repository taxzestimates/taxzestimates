class App.Views.Tax.Archive extends Backbone.View
  templates:
    container: JST['templates/tax/archive']

  initialize: ->
    @currentMonth = ''
    @collection = new App.Collections.Computations
    @setupListeners()

  setupListeners: ->
    @collection.on('sync', @taxInfosLoaded, @)

  render: ->
    @$el.html(@templates.container())
    @collection.fetch()

    $(document).on 'click', '#btn-show-all', () ->
      $("#archive-table tr:hidden").show('fast')
      $(this).hide()

    @

  taxInfosLoaded: ->
    me = @
    @collection.each (model, index) -> me.renderRow(index, model)

    if @collection.length > 10
      $("#archive-table tfoot").removeClass('hide')

  renderRow: (index, model) ->
    modelMonth = moment(model.get('created_at')).format('MMMM')

    if @currentMonth != modelMonth
      @currentMonth = modelMonth
      monthText = @currentMonth
    else
      monthText = ''

    if index >= 10
      hidden = true
    else
      hidden = false

    monthText
    model.view = new App.Views.Tax.ArchiveRow(model: model, monthText: monthText, hidden: hidden)
    @$('tbody').append(model.view.render().$el)
