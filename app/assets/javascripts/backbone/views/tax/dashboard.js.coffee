class App.Views.Tax.Dashboard extends Backbone.View
  templates:
    container: JST['templates/tax/dashboard']
    result: JST['templates/tax/tax_result_box']

  POPOVERS:
    federal: "The <strong>Earned Income Credit</strong> is a U.S tax credit which benefits certain taxpayers who have low income from work in a tax year. The Earned Income Credit reduces the amount of taxes you owe or is refunded to you in the form of a tax refund in the event the Earned Income Credit exceeds the amount owed."
    state: "The rent credit is is available to taxpayers who paid rent and is subject to a tax liability. The <strong>California Rent Credit</strong> is not available to those who receive a tax refund."

  initialize: ->
    @model = tz.latestTax

  render: ->
    @$el.html(@templates.container(model: @model))
    @renderResults()
    @renderCredits()
    @renderTaxSummary()
    @

  afterShow: ->
    @renderIncomeChart()

  renderResults: ->
    @$('.federal-container').html(@templates.result(
      title: 'Federal',
      amount: parseFloat(@model.get('federal_result')).toFixed(2)
    ))

    @$('.state-container').html(@templates.result(
      title: 'State',
      amount: parseFloat(@model.get('state_result')).toFixed(2)
    ))

  renderCredits: ->
    credits = @model.credits()
    _.each credits.federal, (credit) =>
      @$('#federal-tax-credits-table tbody').append("<tr><td>#{credit.text}<span class='glyphicon glyphicon-question-sign' data-container='body' data-toggle='popover' data-placement='right' data-content='" + @POPOVERS.federal + "'></span></td><td class='amount'>#{credit.amount}</td></tr>")

    _.each credits.state, (credit) =>
      @$('#state-tax-credits-table tbody').append("<tr><td>#{credit.text}<span class='glyphicon glyphicon-question-sign' data-container='body' data-toggle='popover' data-placement='right' data-content='" + @POPOVERS.state + "'></span></td><td class='amount'>#{credit.amount}</td></tr>")

    @$('[data-toggle="popover"]').popover({ html: true, trigger: 'hover' })

  renderTaxSummary: ->
    @$('#federal-tax-rate').html(@model.fedTaxRate())
    @$('#state-tax-rate').html(@model.stateTaxRate())
    @$('#effective-tax-rate').html(@model.effectiveTaxRate())

  renderIncomeChart: ->
    chartData = [
        value: @model.takeHomeIncome()
        color: tz.CHART_COLORS['take_home_income']
        highlight: tz.CHART_COLORS['take_home_income_active']
        label: 'Take home income'
      ,
        value: @model.fedTaxes()
        color: tz.CHART_COLORS['federal_taxes']
        highlight: tz.CHART_COLORS['federal_taxes_active']
        label: 'Federal taxes'
      ,
        value: @model.stateTaxes()
        color: tz.CHART_COLORS['state_taxes']
        highlight: tz.CHART_COLORS['state_taxes_active']
        label: 'State taxes'
    ]

    options = {
      percentageInnerCutout: 0
      animationEasing: "easeInOutExpo"
      animateScale: true
      legendTemplate: "<% for (var i=0; i<segments.length; i++){%><div class=\"col-xs-4\"><div class=\"row\"><div class=\"col-xs-3 color-box text-right\"><div class=\"color-swatch\" style=\"background-color: <%=segments[i].fillColor%>\"></div></div><div class=\"col-xs-9 label-box\"><%if(segments[i].label){%><%=segments[i].label%><%}%></div></div></div><%}%>"
      tooltipTemplate: "<%if (label){%><%=label%>: <%}%>$<%= tz.utils.formatCurrency(parseFloat(value).toFixed(2)) %>"
    }

    chartBox = @$('#income-chart').get(0).getContext('2d')
    incomeChart = new Chart(chartBox).Pie(chartData, options)
    @$("#income-chart-legend").html(incomeChart.generateLegend())
