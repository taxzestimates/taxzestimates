class App.Views.Tax.AnnualFiling extends Backbone.View
  templates:
    container: JST['templates/tax/annual_filing']

  initialize: ->
    @model = tz.latestTax

  render: ->
    @$el.html(@templates.container(model: @model))
    @
