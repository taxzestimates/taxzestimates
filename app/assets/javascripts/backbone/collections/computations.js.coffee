class App.Collections.Computations extends Backbone.Collection
  model: App.Models.Computation
  url: '/api/computations'
