class App.Routers.Tax extends Backbone.Router
  routes:
    "": "index"
    "dashboard": "dashboard"
    "options": "recommendations"
    "archive": "archive"
    "annual_filing": "annualFiling"

    "personal_information": "personalInformation"
    "personal_information/:step": "personalInformation"



  initialize: ->
    @mainView = new App.Views.Tax.MainView
    # $('.main-container').html(@mainView.render().$el)

  index: ->
    App.router.navigate('dashboard', trigger: true)

  dashboard: ->
    @mainView.renderDashboard()

  personalInformation: (step) ->
    @mainView.renderPersonalInformation(step)

  recommendations: ->
    @mainView.renderRecommendations()

  archive: ->
    @mainView.renderArchive()

  annualFiling: ->
    @mainView.renderAnnualFiling()
