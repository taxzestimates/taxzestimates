class App.Models.Job extends Backbone.Model
  idAttribute: '_id'

  getID: ->
    @get('_id')

  companyName: ->
    @get('company_name')

  isCurrentlyEmployed: ->
    @get('is_currently_employed')

  dateOfPaystub: ->
    @get('date_of_pay_stub')

  paycheckFrequency: ->
    @get('paycheck_frequency')

  salary: ->
    @get('salary')

  salaryCurrent: ->
    @get 'salary_current'

  bonus: ->
    @get 'bonus'

  massTransportation: ->
    @get 'mass_transportation'

  massTransportationCurrent: ->
    @get 'mass_transportation_current'

  massParkingInvestment: ->
    @get 'mass_parking_investment'

  massParkingInvestmentCurrent: ->
    @get 'mass_parking_investment_current'

  federalTaxesWithheldCurrent: ->
    @get 'federal_taxes_withheld_current'

  federalTaxesWithheld: ->
    @get 'federal_taxes_withheld'

  stateTaxesWithheldCurrent: ->
    @get 'state_taxes_withheld_current'

  stateTaxesWithheld: ->
    @get 'state_taxes_withheld'

  dateText: ->
    moment(@get('date_of_pay_stub') + '/1/' + tz.YEAR).format('MMMM YYYY')

  fedFilingStatus: (jobID) ->
    @get 'federal_filing_status'

  fedAllowances: (jobID) ->
    @get 'federal_allowances'

  fedWithholding: (jobID) ->
    @get 'federal_withholding'

  stateFilingStatus: (jobID) ->
    @get 'state_filing_status'

  stateAllowances: (jobID) ->
    @get 'state_allowances'

  stateWithholding: (jobID) ->
    @get 'state_withholding'

  recommendations: ->
    @get 'recommendations'

  financialImpacts: ->
    @get 'financial_impacts'

  # gusto stuff

  gustoFedFilingStatus: (jobID) ->
    if @fedFilingStatus(jobID)
      tz.FILING_STATUSES[@fedFilingStatus(jobID)]
    else
      'Exempt'

  gustoStateFilingStatus: (jobID) ->
    tz.FILING_STATUSES[@stateFilingStatus(jobID)]

  # PDFs

  federalW4Link: ->
    "/api/jobs/#{@id}/w4"

  stateW4Link: ->
    "/api/jobs/#{@id}/stateW4"
