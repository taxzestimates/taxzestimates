class App.Models.PreTaxContribution extends Backbone.Model

  contributions:
    'mass_transit': 'Mass Transit'
    'mass_parking': 'Mass Parking'
    'ira': 'IRA'

  contributionValues: ->
    _.map(@contributions, (value, key) ->
      [value, key]
    )

