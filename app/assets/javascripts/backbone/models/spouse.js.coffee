class App.Models.Spouse extends Backbone.Model
  idAttribute: '_id'

  firstName: ->
    @get('first_name')

  lastName: ->
    @get('last_name')

  mi: ->
    @get('mi')

  birthMonth: ->
    @get('birth_month')

  birthDay: ->
    @get('birth_day')

  birthYear: ->
    @get('birth_year')

  address1: ->
    @get('address_1')

  address2: ->
    @get('address_2')

  city: ->
    @get('city')

  state: ->
    @get('state')

  zip: ->
    @get('zip')

  sss: ->
    @get('social_security_number')

  incomeType: ->
    @get('income_type')

  jobsCount: ->
    @jobs.length || 0

  fullName: ->
    "#{@firstName()} #{@lastName()}"

  allJobs: ->
    @get('jobs') || 0

  numberOfAllowances: ->
    @get('number_of_allowances')

  retirementAccount: ->
    @get 'retirement_account'

  hasHealthInsurance: ->
    @get 'has_health_insurance'

  monthsWithoutHealthInsurance: ->
    @get 'months_without_health_insurance'

  claimAsDependent: ->
    @get 'claim_as_dependent'

  hasInterestIncome: ->
    @get 'has_interest_income'

  interestIncome: ->
    @get 'interest_income'

  hasUnemploymentIncome: ->
    @get 'has_unemployment_income'

  isReceivingUnemploymentIncome: ->
    @get 'is_receiving_unemployment_income'

  unemploymentIncomeReceivedDate: ->
    @get 'unemployment_income_received_date'

  unemploymentIncome: ->
    @get 'unemployment_income'

  unemploymentIncomeBiWeekly: ->
    @get 'unemployment_income_bi_weekly'

  unemploymentIncomeReceivedDateFormatted: ->
    if @unemploymentIncomeReceivedDate()
      moment(@unemploymentIncomeReceivedDate()).format("L")
    else
      ""

  agi: ->
    tz.utils.formatCurrency(@get('agi'))
