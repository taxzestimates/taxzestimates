class App.Models.Computation extends Backbone.Model
  urlRoot: '/api/computations'
  idAttribute: '_id'

  CURRENTLY_EMPLOYED:
    1: 'Yes',
    0: 'No'

  INCOME_TYPES:
    'employee': 'Employee',
    'contractor': 'Contractor'

  PAYCHECK_FREQUENCIES:
    'twice_a_month': 'Semi-Monthly'
    'once_a_month': 'Once a Month'

  initialize: ->
    @setDependentAndSpouse()
    @setJobs()
    @setSpouseJobs()

  setDependentAndSpouse: ->
    spouseInfo = @get('spouse') || {}
    dependentsInfo = @get('dependents') || []

    if @spouse
      @spouse.clear()
      @spouse.set(spouseInfo)
    else
      @spouse = new App.Models.Spouse(spouseInfo)

    if @dependents
      @dependents.reset(dependentsInfo)
    else
      @dependents = new App.Collections.Dependents(dependentsInfo)

  setJobs: ->
    jobsInfo = @get('jobs') || []

    if @jobs
      @jobs.reset(jobsInfo)
    else
      @jobs = new App.Collections.Jobs(jobsInfo)
      
  setSpouseJobs: ->
    spouseJobsInfo = @spouse.get('jobs') || []

    if @spouse and @spouse.jobs
      @spouse.jobs.reset(spouseJobsInfo)
    else
      @spouse.jobs = new App.Collections.Jobs(spouseJobsInfo)

  toJSON: ->
    data = {}
    data.computation = _.clone(@attributes)

    if @isFilingJoint()
      data.spouse = @spouse.toJSON()

      if @spouse.jobsCount() > 0
        data.spouse.jobs = @spouse.jobs.toJSON()

    if @dependentsCount() > 0
      data.dependents = @dependents.toJSON()

    if @jobsCount() > 0
      data.jobs = @jobs.toJSON()

    data

  saveComputation: ->
    #step = $(document).find("#step").val()

    @save({},
      wait: true
      error: (model, response) =>
        alert = $(document).find("#form-errors")
        errors = $.parseJSON(response.responseText).errors
        count = errors.length
        html = ''

        $(errors).each (index, value) ->
          html += "<li>#{ value }</li>"

        if count > 1
          ctext = "<strong>#{ count }</strong> items require"
        else
          ctext = "item requires"
        
        alert.find('.error-count').html(ctext)
        alert.find('ul').html(html)
        alert.removeClass('hide')
        $("html, body").animate({scrollTop: 0})
      success: (model, response) =>
        # @setDependentAndSpouse() # Need to reset spouse and dependents attributes
        @trigger 'finishSave'
    )

  incomeTypeText: ->
    @INCOME_TYPES[@incomeType()]

  filingStatusText: ->
    tz.FILING_STATUSES[@filingStatus()]

  dependentsCount: ->
    parseInt(@childrenDependents()) + parseInt(@relativesDependents())

  agi: ->
    tz.utils.formatCurrency(@get('agi'))

  federalTaxWithheld: ->
    tz.utils.formatCurrency(@get('federal_tax_withheld'))

  stateTaxWithheld: ->
    tz.utils.formatCurrency(@get('state_tax_withheld'))

  federalResult: ->
    tz.utils.formatCurrency(Math.abs(parseFloat(@get('federal_result')).toFixed(2)))

  stateResult: ->
    tz.utils.formatCurrency(Math.abs(parseFloat(@get('state_result')).toFixed(2)))

  # ANNUALIZED VALUES

  annualizedGrossEarnings: ->
    @get('annualized_gross_earnings')

  annualizedFederalTaxWithheld: ->
    @get('annualized_federal_tax_withheld')

  annualizedStateTaxWithheld: ->
    @get('annualized_state_tax_withheld')

  annualizedPreTaxContributions: ->
    tz.utils.formatCurrency(@get('annualized_pre_tax_contributions'))

  createdAt: ->
    @get('created_at')

  createdAtAmericanFormat: ->
    moment(@createdAt()).format('L')

  amountColorClass: (attr) ->
    amount = @get(attr)
    if amount <= 0 then 'green' else 'red'

  # Attributes

  firstName: ->
    @get('first_name')

  lastName: ->
    @get('last_name')

  mi: ->
    @get('mi')

  birthMonth: ->
    @get('birth_month')

  birthDay: ->
    @get('birth_day')

  birthYear: ->
    @get('birth_year')

  address1: ->
    @get('address_1')

  address2: ->
    @get('address_2')

  city: ->
    @get('city')

  state: ->
    @get('state')

  zip: ->
    @get('zip')

  sss: ->
    @get('social_security_number')

  incomeType: ->
    @get('income_type')

  jobsCount: ->
    @jobs.length || 0

  filingStatus: ->
    @get('filing_status')

  allJobs: ->
    @get('jobs') || 0

  childrenDependents: ->
    @get('children_dependents') || 0

  relativesDependents: ->
    @get('relatives_dependents') || 0

  isFilingJoint: ->
    @filingStatusText() == 'Married Filing Joint'

  hasChildDependent: ->
    parseInt(@childrenDependents()) > 0

  hasRelativeDependent: ->
    parseInt(@relativesDependents()) > 0

  fullName: ->
    "#{@firstName()} #{@lastName()}"

  numberOfAllowances: ->
    @get('number_of_allowances')

  retirementAccount: ->
    @get 'retirement_account'

  hasHealthInsurance: ->
    @get 'has_health_insurance'

  monthsWithoutHealthInsurance: ->
    @get 'months_without_health_insurance'

  claimAsDependent: ->
    @get 'claim_as_dependent'

  hasInterestIncome: ->
    @get 'has_interest_income'

  interestIncome: ->
    @get 'interest_income'

  hasUnemploymentIncome: ->
    @get 'has_unemployment_income'

  isReceivingUnemploymentIncome: ->
    @get 'is_receiving_unemployment_income'

  unemploymentIncomeReceivedDate: ->
    @get 'unemployment_income_received_date'

  unemploymentIncome: ->
    @get 'unemployment_income'

  unemploymentIncomeBiWeekly: ->
    @get 'unemployment_income_bi_weekly'

  unemploymentIncomeReceivedDateFormatted: ->
    if @unemploymentIncomeReceivedDate()
      moment(@unemploymentIncomeReceivedDate()).format("L")
    else
      ""

  dashboardText: ->
    "Your latest tax projections from your " + @dateText() + " computation are as follows:"

  pdfLink: ->
    "/api/computations/#{@id}.pdf"

  form1040ezLink: ->
    "/api/computations/#{@id}/form1040ez"

  form540ezLink: ->
    "/api/computations/#{@id}/form540ez"

  credits: ->
    @get 'credits'

  # Dashboard Stats
  fedTaxRate: ->
    amount = Math.round((parseFloat(@get 'federal_tax_rate') * 100) * 100) / 100
    amount + '%'

  stateTaxRate: ->
    amount = Math.round((parseFloat(@get 'state_tax_rate') * 100) * 100) / 100
    amount + '%'

  effectiveTaxRate: ->
    amount = Math.round((parseFloat(@get 'effective_tax_rate') * 100) * 100) / 100
    amount + '%'

  fedTaxes: ->
    @get 'federal_taxes'

  stateTaxes: ->
    @get 'state_taxes'

  takeHomeIncome: ->
    @get 'take_home_income'

  dateText: ->
    if @get('latest_paycheck')
      moment(@get('latest_paycheck') + '/1/' + tz.YEAR).format('MMMM YYYY')
    else
      ""

  # Disqualifications
  getBanner: ->
    if @get('taxable_income') > tz.MAX_TAXABLE_INCOME
      ['warning', "Oops, your projected taxable income exceeds $#{tz.utils.formatCurrency(tz.MAX_TAXABLE_INCOME)} and disqualifies you from filing a 1040EZ. Tax information provided may be inaccurate."]
    else if (@isFilingJoint() and @get('joint_interest_income') > tz.MAX_INTEREST_INCOME) or (!@isFilingJoint() and @get('actual_interest_income') > tz.MAX_INTEREST_INCOME)
      ['warning', "Oops, your interest income earned for the year exceeds $#{tz.utils.formatCurrency(tz.MAX_INTEREST_INCOME)} and disqualifies you from filing a 1040EZ. Tax information provided may be inaccurate."]
    else if @get('latest_paycheck')
      latest = moment(@get('latest_paycheck') + '/1/' + tz.YEAR)
      now = moment().subtract(1, 'months')

      if now.diff(latest, 'months') >= 1
        ['notice', "Oops, you have not updated your income information for the month of #{now.format('MMMM')}. Tax information provided may be inaccurate.<br>Update your income information now to view your latest tax position and options."]
      else
        false