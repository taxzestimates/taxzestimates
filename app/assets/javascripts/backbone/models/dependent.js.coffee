class App.Models.Dependent extends Backbone.Model

  dependentType: ->
    @get('dependent_type')

  isChild: ->
    @dependentType() is 'child'

  isRelative: ->
    @dependentType() is 'relative'

  firstName: ->
    @get('first_name')

  lastName: ->
    @get('last_name')

  mi: ->
    @get('mi')

  sss: ->
    @get('social_security_number')

  birthMonth: ->
    @get('birth_month')

  birthYear: ->
    @get('birth_year')

  birthDay: ->
    @get('birth_day')