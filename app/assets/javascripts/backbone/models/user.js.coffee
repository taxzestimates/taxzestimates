class App.Models.User extends Backbone.Model
  firstName: ->
    @get('first_name')

  lastName: ->
    @get('last_name')
