class App.Models.DraftComputation extends App.Models.Computation
  urlRoot: '/api/draft_computations'

  toJSON: ->
    data = {}
    data.draft_computation = _.clone(@attributes)

    if @isFilingJoint()
      data.spouse = @spouse.toJSON()

      if @spouse.jobsCount() > 0
        data.spouse.jobs = @spouse.jobs.toJSON()

    if @dependentsCount() > 0
      data.dependents = @dependents.toJSON()

    if @jobsCount() > 0
      data.jobs = @jobs.toJSON()

    data

  currentStep: ->
    @get('current_step') || '1'