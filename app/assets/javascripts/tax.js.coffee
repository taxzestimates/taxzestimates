# TEMPLATES
//= require_tree ./templates/tax

# BACKBONE RELATED, LET'S INCLUDE EVERYTHING FOR NOW!
//= require_tree ./backbone/models
//= require_tree ./backbone/collections
//= require_tree ./backbone/views
//= require_tree ./backbone/routers

