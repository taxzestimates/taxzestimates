$ ->
  Dropzone.autoDiscover = false

  # user avatar upload
  $("#user-photo-preview").dropzone
    headers: {"X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')},
    url: 'users/avatar',
    maxFilesize: 1,
    paramName: "avatar",
    thumbnailWidth: 120,
    thumbnailHeight: 120,
    maxFiles: 1,
    clickable: "#user-photo-upload",
    acceptedFiles: 'image/*',
    init: () ->
      dz = this

      dz.on "success", (file, res) ->
        $("#user-photo-preview").attr('src', res.img.medium)
        $("#user-menu").find(".user-photo").attr('src', res.img.thumb)
        dz.removeFile(file)
        
        alertbox = $("#edit_user").find(".alert")
        alertmsg = alertbox.find(".notice")

        alertmsg.html "Your photo has been updated successfully."
        alertbox
          .removeClass 'alert-danger hide'
          .addClass 'alert-success'
          .show()
        return

      dz.on "uploadprogress", (file, progress) ->
        $("#user-photo-progress .progress-bar").css('width', progress + "%")
        return

      dz.on "sending", (file) ->
        $("#user-photo-progress").css('visibility', 'visible')
        return

      dz.on "complete", (file) ->
        $("#user-photo-progress").css('visibility', 'hidden')
        return

  # handle edit form submit
  $("#edit_user").on 'ajax:success', (e, data, status, xhr) ->
    alertbox = $(this).find(".alert")
    alertmsg = alertbox.find(".notice")

    if data.success
      $("#header-full-name").html [data.user.first_name, data.user.last_name].join(' ')
      alertmsg.html data.success
      alertbox
        .removeClass 'alert-danger'
        .addClass 'alert-success'
    else
      html = '<ul>'
      errors = []

      $.each data, (index, error) ->
        if errors.indexOf(error) == -1
          html += '<li>' + error + '</li>'
          errors.push error
        return
      html += '</ul>'
      alertmsg.html html
      alertbox
        .removeClass 'alert-success'
        .addClass 'alert-danger'

    alertbox
      .removeClass 'hide'
      .show()
    return

  # handle deactivate form submit
  $("#deactivate_user").on 'ajax:success', (e, data, status, xhr) ->
    alertbox = $(this).find(".alert")
    alertmsg = alertbox.find(".notice")

    if data.success
      alertmsg.html data.success
      alertbox
        .removeClass 'alert-danger'
        .addClass 'alert-success'

      window.setTimeout(() ->
        window.location = "/"
      , 5000)
    else
      alertmsg.html data.error
      alertbox
        .removeClass 'alert-success'
        .addClass 'alert-danger'

    alertbox
      .removeClass 'hide'
      .show()
    return

  # hide edit profile modal when showing deactivation modal
  $('#deactivate-account-form').on 'show.bs.modal', (e) ->
    $('#edit-account-form').modal('hide')
    return

  # reset modals
  $(".modal").on 'show.bs.modal', (e) ->
    $(this)
      .find(".alert")
      .addClass('hide')
      .hide()

    $(this)
      .find(".notice")
      .html ""

    $(this)
      .find(".no-fill, [type='password']")
      .val ""

    return
  .on 'shown.bs.modal', (e) ->
    $(this)
      .find("[type='text'], textarea")
      .first()
      .focus()

    return

  return
