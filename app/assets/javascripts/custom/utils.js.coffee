$ ->
  tz.utils = {}

  tz.utils.formatNumber = (num) ->
    num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")

  tz.utils.formatCurrency = (num) ->
    tz.utils.formatNumber(num)

  tz.utils.dropdownString = (values) ->
    # VALUE IS AN ARRAY!!
    html = ''
    _.each(values, (value) ->
      html += "<option value=#{value[1]}>#{value[0]}</option>"
    )
    html

  tz.utils.select2data = (hash) ->
    _.map(hash, (v, k) ->
      {id: k, text: v}
    )

  tz.utils.statesDropdownData = tz.utils.select2data(tz.STATES)
  tz.utils.monthsDropdownData = tz.utils.select2data(tz.MONTHS)
  tz.utils.yearsDropdownData = (
    year = new Date().getFullYear()
    _.map(_.range(year, year-70, -1), (v) ->
      year = v.toString()
      {id: year, text: year}
    )
  )
  tz.utils.daysDropdownData = (
    _.map(_.range(1, 32), (v) ->
      day = v.toString()
      {id: day, text: day}
    )
  )

  String.prototype.format = ->
    args = arguments
    return this.replace /{(\d+)}/g, (match, number) ->
      return if typeof args[number] isnt 'undefined' then args[number] else match

  # # ADDITIONALS
  # $.fn.serializeObject = ->
  #   o = {};
  #   a = this.serializeArray();
  #   $.each(a, ->
  #     if (o[this.name] != undefined)
  #       if !o[this.name].push
  #         o[this.name] = [o[this.name]]
  #       o[this.name].push(this.value || '')
  #     else
  #       o[this.name] = this.value || ''
  #   )
  #   return o
