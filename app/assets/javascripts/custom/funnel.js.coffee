$ ->
  $('#join-list-btn').click((e) ->
    e.preventDefault();
    email = $('#join-list-email').val()
    $.ajax
      url: '/api/invitations/save_invite'
      method: 'POST'
      data:
        email: email
      success: (resp) =>
        $alert = $('.alert-container')
        $alert.removeClass('alert-success alert-danger')
        if resp.success
          $alert.addClass('alert-success').html("You've successfully joined the waiting list!<br/>Be on the lookout for our periodic newsletters and product launch.")
          $('#join-list-email').val('')
        else
          msg = resp.errors.join(', ')
          msg = 'This email has already been added to the waiting list!' if msg == 'Email has already been taken'
          msg = 'Please enter your email to join.' if msg == "Email can't be blank, Email Please enter valid email."
          msg = 'Please enter a valid email.' if msg == "Email Please enter valid email."

          $alert.addClass('alert-danger').html(msg)

        if $alert.is(':hidden')
          $alert.fadeIn()
        else
          $alert.show()
  )

  $('#join-list-email').keypress((e) ->
    if e.which == 13
      $('#join-list-btn').click()
  )
