$ ->
  # enable/disable interest income fields
  $(document).on 'change', 'input[type=radio][name="has_interest_income"]', (e) ->
    self = $("#self-interest-income")
    spouse = $("#spouse-interest-income")
    val = this.value

    if val in ['self', 'both']
      self
        .removeClass('disabled')
        .find('input')
        .prop('disabled', false)
    else
      self
        .addClass('disabled')
        .find('input')
        .val("")
        .prop('disabled', true)

    if val in ['spouse', 'both']
      spouse
        .removeClass('disabled')
        .find('input')
        .prop('disabled', false)
    else
      spouse
        .addClass('disabled')
        .find('input')
        .val("")
        .prop('disabled', true)

    return

  # enable/disable unemployment income fields
  $(document).on 'change', 'input[type=radio][name="has_unemployment_income"]', (e) ->
    self = $("#self-unemployment-income-box")
    spouse = $("#spouse-unemployment-income-box")
    val = this.value

    if val in ['self', 'both']
      self
        .prop('disabled', false)
        .find('.input-group')
        .removeClass('disabled')
    else
      self
        .prop('disabled', true)
        .find('.input-group')
        .addClass('disabled')
      
      self
        .find('input[type="number"]')
        .val("")

    $('input[type=radio][name="self_is_receiving_unemployment_income"]:checked').change();

    if val in ['spouse', 'both']
      spouse
        .prop('disabled', false)
        .find('.input-group')
        .removeClass('disabled')
    else
      spouse
        .prop('disabled', true)
        .find('.input-group')
        .addClass('disabled')
      
      spouse
        .find('input[type="number"]')
        .val("")

    $('input[type=radio][name="spouse_is_receiving_unemployment_income"]:checked').change();

    return

  # enable/disable bi-weekly self unemployment income field
  $(document).on 'change', 'input[type=radio][name="self_is_receiving_unemployment_income"]', (e) ->
    fieldset = this.closest('fieldset')
    field = $('#self-unemployment-income-bi-weekly');
    val = parseInt this.value

    if val == 1 && $(fieldset).prop('disabled') == false
      field
        .removeClass('disabled')
        .find('input')
        .prop('disabled', false)
    else
      field
        .addClass('disabled')
        .find('input')
        .prop('disabled', true)

    return

  # enable/disable bi-weekly spouse unemployment income field
  $(document).on 'change', 'input[type=radio][name="spouse_is_receiving_unemployment_income"]', (e) ->
    fieldset = this.closest('fieldset')
    field = $('#spouse-unemployment-income-bi-weekly');
    val = parseInt this.value

    if val == 1 && $(fieldset).prop('disabled') == false
      field
        .removeClass('disabled')
        .find('input')
        .prop('disabled', false)
    else
      field
        .addClass('disabled')
        .find('input')
        .prop('disabled', true)

    return

  return