$ ->
  $.validator.setDefaults
    highlight: (element) ->
      if $(element).prev('.select2-container').length
        $(element).prev('.select2-container').addClass('error')
      $(element).addClass 'error'
      return
    unhighlight: (element) ->
      if $(element).prev('.select2-container').length
        $(element).prev('.select2-container').removeClass('error')
      $(element).removeClass 'error'
      return
    errorPlacement: (error, element) ->
      # do nothing
      return
