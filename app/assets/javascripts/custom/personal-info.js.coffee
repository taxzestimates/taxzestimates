$ ->
  # enable/disable consecutive months without health insurance field
  $(document).on 'change', 'input[type=radio][name="has_health_insurance"]', (e) ->
    field = $('#months-without-health-insurance');
    val = parseInt this.value

    if val == 1
      field
        .val("")
        .prop('disabled', true)
    else
      field
        .prop('disabled', false)
    return

  # hide alert
  $(document).on 'click', '.hide-alert', (e) ->
    $(this).closest('.alert').addClass('hide')
    return

  # focus effect on all input groups
  $(document).on
    'focus': (e) ->
      $(@).siblings('.input-group-addon').addClass('focus')
      return
    'blur': (e) ->
      $(@).siblings('.input-group-addon').removeClass('focus')
      return
    '.input-group input'

  return
