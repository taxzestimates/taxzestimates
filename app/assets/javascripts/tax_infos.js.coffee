# TEMPLATES
//= require_directory ./templates/tax_infos

# BACKBONE RELATED
//= require backbone/models/tax_info
//= require backbone/models/pre_tax_contribution
//= require backbone/collections/tax_infos
//= require backbone/collections/pre_tax_contributions
//= require_directory ./backbone/views/tax_infos
//= require backbone/routers/tax_info
