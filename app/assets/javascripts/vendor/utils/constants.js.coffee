$ ->
  tz.API = '/api/computations'
  
  tz.YEAR = 2015

  tz.ISRP =
    'adult': 27
    'child': 13

  tz.STANDARD_DEDUCTION =
    'single': 6300
    'joint': 12600

  tz.PERSONAL_EXEMPTION =
    'single': 4000
    'joint': 8000

  tz.MAX_TAXABLE_INCOME = 100000

  tz.MAX_INTEREST_INCOME = 1500

  tz.FILING_STATUSES =
    'single': 'Single',
    # 'head_of_household': 'Head of Household',
    'married_filing_joint': 'Married Filing Joint'
    # 'married_filing_separate': 'Married Filing Separate',
    # 'surviving_spouse': 'Surviving Spouse',
    # 'exempt': 'Exempt'

  tz.CHART_COLORS =
    'take_home_income': '#a1c638',
    'take_home_income_active': '#b5ca78',
    'federal_taxes': '#e53535',
    'federal_taxes_active': '#ea7a7b',
    'state_taxes': '#e19b21',
    'state_taxes_active': '#eeba5f',
    'highlight': '#fff'
  
  tz.STATES =
    'AL': 'Alabama'
    'AK': 'Alaska'
    'AS': 'American Samoa'
    'AZ': 'Arizona'
    'AR': 'Arkansas'
    'CA': 'California'
    'CO': 'Colorado'
    'CT': 'Connecticut'
    'DE': 'Delaware'
    'DC': 'District of Columbia'
    'FM': 'Federated States of Micronesia'
    'FL': 'Florida'
    'GA': 'Georgia'
    'GU': 'Guam'
    'HI': 'Hawaii'
    'ID': 'Idaho'
    'IL': 'Illinois'
    'IN': 'Indiana'
    'IA': 'Iowa'
    'KS': 'Kansas'
    'KY': 'Kentucky'
    'LA': 'Louisiana'
    'ME': 'Maine'
    'MH': 'Marshall Islands'
    'MD': 'Maryland'
    'MA': 'Massachusetts'
    'MI': 'Michigan'
    'MN': 'Minnesota'
    'MS': 'Mississippi'
    'MO': 'Missouri'
    'MT': 'Montana'
    'NE': 'Nebraska'
    'NV': 'Nevada'
    'NH': 'New Hampshire'
    'NJ': 'New Jersey'
    'NM': 'New Mexico'
    'NY': 'New York'
    'NC': 'North Carolina'
    'ND': 'North Dakota'
    'MP': 'Northern Mariana Islands'
    'OH': 'Ohio'
    'OK': 'Oklahoma'
    'OR': 'Oregon'
    'PW': 'Palau'
    'PA': 'Pennsylvania'
    'PR': 'Puerto Rico'
    'RI': 'Rhode Island'
    'SC': 'South Carolina'
    'SD': 'South Dakota'
    'TN': 'Tennessee'
    'TX': 'Texas'
    'UT': 'Utah'
    'VT': 'Vermont'
    'VI': 'Virgin islands'
    'VA': 'Virginia'
    'WA': 'Washington'
    'WV': 'West virginia'
    'WI': 'Wisconsin'
    'WY': 'Wyoming'

  tz.MONTHS =
    '1': 'January'
    '2': 'February'
    '3': 'March'
    '4': 'April'
    '5': 'May'
    '6': 'June'
    '7': 'July'
    '8': 'August'
    '9': 'September'
    '10': 'October'
    '11': 'November'
    '12': 'December'
