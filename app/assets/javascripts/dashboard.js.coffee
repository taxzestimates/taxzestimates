$ ->
  # store current data here
  tz.taxInfo = {}

  saveCurrentData = ($form) ->
    _.each($form.serializeArray(), (keyVal) ->
      tz.taxInfo[keyVal.name] = keyVal.value
    )

  submitTaxInfo = ->
    $.ajax
      url: '/api/tax_infos'
      method: 'POST'
      data:
        tax_info: tz.taxInfo
      success: (resp) =>
        $('.step3').removeClass('current').addClass('done')
        federalRefund = tz.utils.formatNumber(resp.federal_refund)
        stateRefund = tz.utils.formatNumber(resp.state_refund)
        $container = $('.refund-container')
        $container.find('.federal-refund .amount').text(federalRefund)
        $container.find('.state-refund .amount').text(stateRefund)
        $container.find('.loading-animation').hide()

  $('.dashboard-form').submit((e) ->
    e.preventDefault()
    $form = $(e.currentTarget)
    saveCurrentData($form)
    currentStep = $form.data('step')
    nextStep = currentStep + 1
    $form.hide()

    $(".step").removeClass('current')
    $(".step#{currentStep}").addClass('done')
    $(".step#{nextStep}").addClass('current')
    $("form[data-step=#{nextStep}]").show()
    if $form.data('last-form')
      submitTaxInfo()
  )

  $('input[type=radio]').change ->
    if @value == 'option1'
      $('.opt1').show()
      $('.opt2').hide()
      $('label.payprov').css 'margin-top', '20px'
    else if @value == 'option2'
      $('.opt2').show()
      $('.opt1').hide()
      $('label.payprov').css 'margin-top', '0px'
    event.preventDefault()
    false

