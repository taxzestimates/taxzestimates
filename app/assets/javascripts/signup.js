$(document).ready(function() {
	$("#new_user").submit(function() {
		var form = $(this);
		var submit = $(this).find('[type=submit]');
		var notice = form.find('.notice-box');
		var msg = notice.find('.notice');

		if($("#terms").prop('checked') === false) {
			alert('You must agree to the terms if you want to use the service.');
		} else {
			data = form.serialize();

			submit.prop('disabled', true).val("").addClass('btn-loading');

			$.post('/users', data, function(response) {
				if(response.success) {
					msg.html(response.success);
					notice.removeClass('alert-danger hide').addClass('alert-success');
					form.find('input[type=text], input[type=email], input[type=password]').val("");
					form.find('input[type=checkbox]').prop('checked', false);
				} else {
					errors = '<ul>';

					$.each(response, function(index, error) {
						errors += '<li>' + error + '</li>';
					});

					errors += '</ul>';

					msg.html(errors);
					notice.removeClass('alert-success hide').addClass('alert-danger');
				}

				submit.prop('disabled', false).val("Create your Account").removeClass('btn-loading');
			});
		}

		return false;
	});
});