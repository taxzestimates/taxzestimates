$(document).ready(function() {
	if(typeof(is_home) !== "undefined") {
		$('.navbar-nav').onePageNav({
			scrollOffset: 60,
			easing: 'easeInOutExpo',
			filter: ':not(.external)'
		});
	}

	$('.lnkvid').venobox({
	    framewidth: '800px',
	    frameheight: '452px'
	});

	$("img.tzlogo").click(function() {
	  	$('body, html').animate({scrollTop: 0}, 800);
	});
});