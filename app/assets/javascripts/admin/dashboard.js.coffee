$ ->
  chartFont = '"Proxima Nova W01", Helvetica, Arial, sans-serif'

  if typeof data != 'undefined'
    $("[name='metric']").select2( minimumResultsForSearch: Infinity)
    $("[name='period']").select2( minimumResultsForSearch: Infinity)

    graphBg = "#cee3b9"
    graphFg = "#a1c977"
    graphStroke = "#fff"

    # initial graph data
    graphData =
      labels: labels
      datasets: [
      	label: "User Growth"
      	fillColor: graphBg
      	strokeColor: graphFg
      	pointColor: graphFg
      	pointStrokeColor: graphStroke
      	pointHighlightFill: graphStroke
      	pointHighlightStroke: graphFg
      	data: data
      ]

    graphArea = $('#graph').get(0).getContext('2d')
    theGraph = new Chart(graphArea).Line(graphData, {
      tooltipFontFamily: chartFont
      tooltipTitleFontFamily: chartFont
      scaleFontFamily: chartFont
    })

    # update data based on filters
    $("#filters select").change () ->
      metric = $("[name='metric']").select2('val')
      period = $("[name='period']").select2('val')

      $.post refresh_path, { metric: metric, period, period}, (response) ->
        graphData =
          labels: response.labels
          datasets: [
            label: response.title
            fillColor: graphBg
            strokeColor: graphFg
            pointColor: graphFg
            pointStrokeColor: graphStroke
            pointHighlightFill: graphStroke
            pointHighlightStroke: graphFg
            data: response.data
          ]

        theGraph.destroy()
        theGraph = new Chart(graphArea).Line(graphData, {
          scaleLabel: if metric == 'user-growth' then "<%=value%>%" else "<%=value%>"
          tooltipTemplate: "<% if (label) { %><%=label%>: <% } %><%=value%>" + (if metric == 'user-growth' then '%' else '')
          tooltipFontFamily: chartFont
          tooltipTitleFontFamily: chartFont
          scaleFontFamily: chartFont
        })

      return

