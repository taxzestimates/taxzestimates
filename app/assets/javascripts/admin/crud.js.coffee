$ ->
  $(document).on 'click', '.toggle-row', (e) ->
    e.preventDefault()
    msgBox("Are you sure you want to " + $(@).data('form-action') + " this " + $(@).data('type') + "?", $(@).data('form-action'), $(@).data('form-method'), $(@).data('id'), toggle_path)

  $(document).on 'click', '.set-primary', (e) ->
    e.preventDefault()
    msgBox("Are you sure you want to set this as the primary " + $(@).data('type') + "?", "primary", $(@).data('form-method'), $(@).data('id'), set_primary_path)

  $(document).on 'click', '.delete-row', (e) ->
    e.preventDefault()
    msgBox("Are you sure you want to delete this " + $(@).data('type') + "?", "delete", $(@).data('form-method'), $(@).data('id'), data_path + "/" + $(@).data('id'))



  # show message box
  msgBox = (message, action, method, id, path) ->
    box = $("#message-box")
    box.find(".message").html(message)

    if action
      $("#btn-confirm").html("Yes").data('form-action', action).data('form-method', method).data('id', id).data('path', path)
      $("#btn-deny").show()
    else
      $("#btn-confirm").html("OK").data('form-action', false).data('form-method', false).data('id', false).data('path', false)
      $("#btn-deny").hide()

    box.modal('show')

  $("#btn-confirm").click (e) ->
    $.ajax
      url: $(@).data('path')
      method: $(@).data('form-method')
      data:
        toggle: $(@).data('form-action')
        id: $(@).data('id')
      success: (resp) =>
        location.reload()

  # show add form
  $("#add-item").click (e) ->
    modal = $("#data-form")
    modal.find('form').prop('method', 'post')
    modal.find(".modal-title").html("Add Item")
    modal.find("input, select, textarea").html("")
    modal.modal('show')

  # show delete form
  $(document).on 'click', '.edit-row', (e) ->
    e.preventDefault()
    modal = $("#data-form")
    id = $(@).data('id')

