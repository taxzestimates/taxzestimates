$ ->
  $(document).on 'click', '.toggle-row', (e) ->
    e.preventDefault()
    msgBox("Are you sure you want to " + $(@).data('action') + " this user?", $(@).data('action'), $(@).data('id'))

  $("#btn-confirm").click (e) ->
    if $(@).data('action')
      $.ajax
        url: toggle_path
        method: 'PUT'
        data:
          toggle: $(@).data('action')
          id: $(@).data('id')
        success: (resp) =>
          location.reload()

  # show message box
  msgBox = (message, action, id) ->
    box = $("#message-box")
    box.find(".message").html(message)

    if action
      $("#btn-confirm").html("Yes").data('action', action).data('id', id)
      $("#btn-deny").show()
    else
      $("#btn-confirm").html("OK").data('action', false).data('id', false)
      $("#btn-deny").hide()

    box.modal('show')
