require 'pdf_forms'

class PDFGenerator::W4
  include ActionView::Helpers::NumberHelper

  W4_PDF_PATH = Rails.public_path.join('pdfs','fw4_custom.pdf')

  PDF_FORM_FIELDS_MAPPING = {
      first_name: 'topmostSubform[0].Page1[0].Line1[0].f1_09_0_[0]',
      last_name: 'topmostSubform[0].Page1[0].Line1[0].f1_10_0_[0]',
      social_security_number: 'topmostSubform[0].Page1[0].f1_13_0_[0]',
      home_address: 'topmostSubform[0].Page1[0].Line1[0].f1_11_0_[0]',
      city_state_zip: 'topmostSubform[0].Page1[0].Line1[0].f1_12_0_[0]',
      additional_amount_to_withhold: 'topmostSubform[0].Page1[0].f1_15_0_[0]',
      exemption: 'topmostSubform[0].Page1[0].f1_16_0_[0]',
      signature: 'topmostSubform[0].Page1[0].tz_employee_signature[0]',
      date: 'topmostSubform[0].Page1[0].tz_date[0]',
      allowances: 'topmostSubform[0].Page1[0].f1_14_0_[0]'
  }

  attr_reader :file_name, :file_path

  def initialize(job)
    @pdftk = PdfForms.new(Settings.pdftk_path)
    @job = job
    @computation = @job.person
    generate_pdf_data
    generate_pdf
  end

  private

  def generate_pdf_data
    w4_attributes = {
        first_name: @computation.first_name,
        last_name: @computation.last_name,
        social_security_number: @computation.social_security_number,
        home_address: @computation.home_address,
        city_state_zip: @computation.city_state_zip,
        signature: [@computation.first_name, @computation.last_name].join(' '),
        date: Date.current.to_s,
        allowances: @job.federal_allowances
    }

    recommended_amount_to_withhold = @job.recommended_federal_withholding

    w4_attributes[:exemption], w4_attributes[:additional_amount_to_withhold] = if recommended_amount_to_withhold.zero?
      ['Exempt', '']
    else
      ['', number_to_currency(recommended_amount_to_withhold, :unit => "")]
    end

    @pdf_data = {}
    PDF_FORM_FIELDS_MAPPING.each do |tz_attribute, pdf_field_name|
      @pdf_data[pdf_field_name] = w4_attributes[tz_attribute]
    end

    if @computation.filing_status == 'single'
      @pdf_data['topmostSubform[0].Page1[0].c1_01[0]'] = '1'
    else
      @pdf_data['topmostSubform[0].Page1[0].c1_01[1]'] = '2'
    end
  end

  def generate_pdf
    @file_name = "#{@job.id}_w4.pdf"
    @file_path = Rails.root.join('tmp', @file_name)
    @pdftk.fill_form W4_PDF_PATH, @file_path, @pdf_data, flatten: true
  end
end
