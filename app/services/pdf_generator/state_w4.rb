require 'pdf_forms'

class PDFGenerator::StateW4
  include ActionView::Helpers::NumberHelper

  W4_PDF_PATH = Rails.public_path.join('pdfs','ca_w4.pdf')

  # ["Full Name", "Address", "City, State, Zip", "SSN", "S", "1A", "1B", "A+B", "C", "D", "Date", "ER Name &amp; Address", "ER Account#"]

  PDF_FORM_FIELDS_MAPPING = {
    full_name: 'Full Name',
    social_security_number: 'SSN',
    city_state_zip: 'City, State, Zip',
    home_address: 'Address',
    filing_status: 'S',
    one: '1A',
    two: 'C',
    three: 'D'
  }

  attr_reader :file_name, :file_path

  def initialize(job)
    @pdftk = PdfForms.new(Settings.pdftk_path)
    @job = job
    @computation = @job.person
    generate_pdf_data
    generate_pdf
  end

  private

  def generate_pdf_data
    w4_attributes = {
      full_name: "#{@computation.first_name} #{@computation.last_name}",
      social_security_number: @computation.social_security_number,
      home_address: [@computation.address_1, @computation.address_2].join(', '),
      city_state_zip: [@computation.city, @computation.state, @computation.zip].join(', '),
      filing_status: 'Opt1'
    }

    recommendation = @job.recommended_state_withholding

    # one
    w4_attributes[:one] = @job.state_allowances

    # two
    w4_attributes[:two] = number_to_currency(recommendation)

    # three
    w4_attributes[:three] = recommendation > 0 ? 'Off' : 'Yes'

    @pdf_data = {}
    PDF_FORM_FIELDS_MAPPING.each do |tz_attribute, pdf_field_name|
      @pdf_data[pdf_field_name] = w4_attributes[tz_attribute]
    end
  end

  def generate_pdf
    @file_name = "#{@job.id}_state_w4.pdf"
    @file_path = Rails.root.join('tmp', @file_name)
    @pdftk.fill_form W4_PDF_PATH, @file_path, @pdf_data, flatten: true
  end
end
