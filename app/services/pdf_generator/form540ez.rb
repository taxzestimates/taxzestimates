require 'pdf_forms'
include ActionView::Helpers::NumberHelper

class PDFGenerator::Form540ez
  PDF_PATH = Rails.public_path.join('pdfs', '14_540.pdf')

  PDF_FORM_FIELDS_MAPPING = {
    first_name: '103',
    last_name: '105',
    mi: '104',
    ssn: '107',
    spouse_first_name: '108',
    spouse_last_name: '110',
    spouse_mi: '109',
    spouse_ssn: '112',
    address: '115',
    city: '118',
    state: '119',
    zip: '120',
    date_of_birth: '124',
    spouse_date_of_birth: '125',
    filing_status: '128',
    claim_as_dependent: '130',
    full_name: '105',

    line7_personal: '131',
    line7_personal_exemptions: '132',
    line9_senior: '135',
    line9_senior_exemptions: '136',
    line11_exemptions: '151',
    line12_state_wages: '201',
    line13_federal_agi: '202',
    line14_ca_subtractions: '203',
    line15_agi_after_subtractions: '204',
    line16_ca_additions: '205',
    line17_state_agi: '206',
    line18_ca_deductions: '207',
    line19_taxable_income: '208',
    line31_tax_table_box: '209',
    line31_tax_table_amount: '210',
    line32_exemption_credits: '211',
    line33_tax_after_credits: '212',
    line35_tax_after_tax2: '215',
    line40_child_dependent_credit: '216',
    line43_credit_amount: '219',
    line44_credit_amount: '222',
    line45_two_credits: '223',
    line46_rent_credit: '224',
    line47_total_credits: '225',
    line48_tax_after_credits: '226',
    line61_alt_min_tax: '301',
    line62_mental_health_tax: '302',
    line63_credit_recapture: '303',
    line64_tax_after_alt_taxes: '304',
    line71_annualized_tax_withheld: '305',
    line72_ca_other_payments: '306',
    line73_real_estate: '307',
    line74_excess_sdi: '308',
    line75_total_payments: '309',
    line91_state_refund: '310',
    line92_next_year_tax: '311',
    line93_overpaid_tax: '312',
    line94_state_liability: '313',
    line110_total_contribution: '420',
    line111_total_amount_owed: '501',
    line115_total_refund: '506'
  }

  attr_reader :file_name, :file_path

  def initialize(computation)
    @pdftk = PdfForms.new(Settings.pdftk_path)
    @computation = computation
    generate_pdf_data
    generate_pdf
  end

  private

  def generate_pdf_data
    state = BaseComputation::CAStateResult.new(@computation)

    attributes = {
      first_name: @computation.first_name,
      last_name: @computation.last_name,
      mi: @computation.middle_initial,
      ssn: @computation.social_security_number,
      spouse_first_name: @computation.spouse.try(:first_name),
      spouse_last_name: @computation.spouse.try(:last_name),
      spouse_ssn: @computation.spouse.try(:social_security_number),
      address: @computation.home_address,
      city: @computation.city,
      state: @computation.state,
      zip: @computation.zip,
      date_of_birth: @computation.birth_date.strftime('%m%d%Y'),
      spouse_date_of_birth: @computation.is_filing_joint? ? @computation.spouse.birth_date.strftime('%m%d%Y') : nil,
      filing_status: @computation.is_filing_joint? ? 'MFJ' : 'Single',
      claim_as_dependent: @computation.claim_as_dependent ? 'On' : 'Off',
      full_name: @computation.full_name,

      line7_personal: state.personal_status,
      line7_personal_exemptions: state.personal_exemptions ? number_to_currency(state.personal_exemptions) : nil,
      line9_senior: state.senior_status,
      line9_senior_exemptions: state.senior_exemptions ? number_to_currency(state.senior_exemptions) : nil,
      line11_exemptions: state.exemptions ? number_to_currency(state.exemptions) : nil,
      line12_state_wages: number_to_currency(@computation.filing_status == 'single' ? @computation.wages_salaries : @computation.joint_wages_salaries, :precision => 0),
      line13_federal_agi: number_to_currency(@computation.filing_status == 'single' ? @computation.agi : @computation.joint_agi, :precision => 0),
      line14_ca_subtractions: number_to_currency(@computation.filing_status == 'single' ? @computation.actual_unemployment_income : @computation.joint_actual_unemployment_income, :precision => 0),
      line15_agi_after_subtractions: number_to_currency(state.agi_after_subtractions, :precision => 0),
      line16_ca_additions: number_to_currency(0, :precision => 0),
      line17_state_agi: number_to_currency(state.state_agi, :precision => 0),
      line18_ca_deductions: number_to_currency(state.standard_deduction, :precision => 0),
      line19_taxable_income: number_to_currency(state.taxable_income, :precision => 0),
      line31_tax_table_box: 'TT',
      line31_tax_table_amount: number_to_currency(state.tax_due_before_exemptions, :precision => 0),
      line32_exemption_credits: state.exemptions ? number_to_currency(state.exemptions, :precision => 0) : nil,
      line33_tax_after_credits: number_to_currency(state.tax_due, :precision => 0),
      line35_tax_after_tax2: number_to_currency(state.tax_due, :precision => 0),
      line40_child_dependent_credit: number_to_currency(0, :precision => 0),
      line43_credit_amount: number_to_currency(0, :precision => 0),
      line44_credit_amount: number_to_currency(0, :precision => 0),
      line45_two_credits: number_to_currency(0, :precision => 0),
      line46_rent_credit: number_to_currency(state.rent_credit, :precision => 0),
      line47_total_credits: number_to_currency(state.rent_credit, :precision => 0), # only rent credit for now
      line48_tax_after_credits: number_to_currency(state.tax_after_credits > 0 ? state.tax_after_credits : 0, :precision => 0),
      line61_alt_min_tax: number_to_currency(0, :precision => 0),
      line62_mental_health_tax: number_to_currency(0, :precision => 0),
      line63_credit_recapture: number_to_currency(0, :precision => 0),
      line64_tax_after_alt_taxes: number_to_currency(state.tax_after_credits > 0 ? state.tax_after_credits : 0, :precision => 0), # use tax after credits for now since no "other taxes"
      line71_annualized_tax_withheld: number_to_currency(state.income_tax_withheld, :precision => 0),
      line72_ca_other_payments: number_to_currency(0, :precision => 0),
      line73_real_estate: number_to_currency(0, :precision => 0),
      line74_excess_sdi: number_to_currency(0, :precision => 0),
      line75_total_payments: number_to_currency(state.income_tax_withheld, :precision => 0),
      line91_state_refund: number_to_currency(state.state_refund > 0 ? state.state_refund : 0, :precision => 0),
      line92_next_year_tax: number_to_currency(0, :precision => 0),
      line93_overpaid_tax: number_to_currency(0, :precision => 0),
      line94_state_liability: number_to_currency(state.state_liability > 0 ? state.state_liability : 0, :precision => 0),
      line110_total_contribution: number_to_currency(0, :precision => 0),
      line111_total_amount_owed: state.state_liability > 0 ? number_to_currency(state.state_liability, :precision => 0) : nil,
      line115_total_refund: state.state_liability < 0 ? number_to_currency(state.state_liability.abs, :precision => 0) : nil
    }

    @pdf_data = {}
    PDF_FORM_FIELDS_MAPPING.each do |tz_attribute, pdf_field_name|
      @pdf_data[pdf_field_name] = attributes[tz_attribute]
    end
  end

  def generate_pdf
    @file_name = "#{@computation.id}_540ez.pdf"
    @file_path = Rails.root.join('tmp', @file_name)
    @pdftk.fill_form PDF_PATH, @file_path, @pdf_data, flatten: true
  end
end
