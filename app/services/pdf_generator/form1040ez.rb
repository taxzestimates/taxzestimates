require 'pdf_forms'
include ActionView::Helpers::NumberHelper

class PDFGenerator::Form1040ez
  PDF_PATH = Rails.public_path.join('pdfs','form1040ez.pdf')

  PDF_FORM_FIELDS_MAPPING = {
    first_name: 'topmostSubform[0].Page1[0].Entity[0].p1-t4[0]',
    last_name: 'topmostSubform[0].Page1[0].Entity[0].p1-t5[0]',
    sss: 'topmostSubform[0].Page1[0].Entity[0].p1-t11[0]',
    spouse_first_name: 'topmostSubform[0].Page1[0].Entity[0].p1-t6[0]',
    spouse_last_name: 'topmostSubform[0].Page1[0].Entity[0].p1-t7[0]',
    spouse_sss: 'topmostSubform[0].Page1[0].Entity[0].SpousesSSN[0].p1-t14[0]',
    address: 'topmostSubform[0].Page1[0].Entity[0].p1-t8[0]',
    city_state_zip: 'topmostSubform[0].Page1[0].Entity[0].p1-t10[0]',
    income_1: 'topmostSubform[0].Page1[0].f1_014_0_[0]',
    income_2: 'topmostSubform[0].Page1[0].f1_016_0_[0]',
    income_3: 'topmostSubform[0].Page1[0].f1_018_0_[0]',
    income_4: 'topmostSubform[0].Page1[0].f1_020_0_[0]',
    income_5_you: 'topmostSubform[0].Page1[0].#subform[4].c1_03[0]',
    income_5_spouse: 'topmostSubform[0].Page1[0].#subform[4].c1_04[0]',
    income_5: 'topmostSubform[0].Page1[0].f1_022_0_[0]',
    income_6: 'topmostSubform[0].Page1[0].f1_024_0_[0]',
    pct_7: 'topmostSubform[0].Page1[0].f1_026_0_[0]',
    pct_8a: 'topmostSubform[0].Page1[0].f1_034_0_[0]',
    pct_8b: 'topmostSubform[0].Page1[0].f1_031_0_[0]',
    pct_9: 'topmostSubform[0].Page1[0].f1_032_0_[0]',
    pct_10: 'topmostSubform[0].Page1[0].f1_032_0_[1]',
    pct_11_check: 'topmostSubform[0].Page1[0].c1_05[0]',
    pct_11: 'topmostSubform[0].Page1[0].f1_036_0_[0]',
    pct_12: 'topmostSubform[0].Page1[0].f1_036_0_[1]',

    refund_13a: 'topmostSubform[0].Page1[0].f1_038_0_[0]',
    amount_you_owe: 'topmostSubform[0].Page1[0].f1_066_0_[0]'

  }

  attr_reader :file_name, :file_path

  def initialize(computation)
    @pdftk = PdfForms.new(Settings.pdftk_path)
    @computation = computation
    generate_pdf_data
    generate_pdf
  end

  private

  def generate_pdf_data
    fed = BaseComputation::FederalResult.new(@computation)

    attributes = {
      first_name: @computation.first_name,
      last_name: @computation.last_name,
      sss: @computation.social_security_number,
      spouse_first_name: @computation.spouse.try(:first_name),
      spouse_last_name: @computation.spouse.try(:last_name),
      spouse_sss: @computation.spouse.try(:social_security_number),
      address: @computation.home_address,
      city_state_zip: @computation.city_state_zip,
      income_1: number_to_currency(!@computation.is_filing_joint? ? @computation.wages_salaries : @computation.joint_wages_salaries),
      income_2: number_to_currency(!@computation.is_filing_joint? ? @computation.actual_interest_income : @computation.joint_interest_income),
      income_3: number_to_currency(!@computation.is_filing_joint? ? @computation.actual_unemployment_income : @computation.joint_actual_unemployment_income),
      income_4: number_to_currency(!@computation.is_filing_joint? ? @computation.agi : @computation.joint_agi),
      income_5: number_to_currency(fed.standard_deduction + fed.personal_deduction),
      income_5_you: @computation.claim_as_dependent ? 'Yes' : 'Off',
      income_5_spouse: @computation.spouse.try(:claim_as_dependent) ? 'Yes' : 'Off',
      income_6: number_to_currency(fed.taxable_income),
      pct_7: number_to_currency(fed.income_tax_withheld),
      pct_8a: number_to_currency(fed.earned_income_credit),
      pct_8b: '0',
      pct_9: number_to_currency(fed.income_tax_withheld + fed.earned_income_credit),
      pct_10: number_to_currency(fed.tax_due),
      pct_11_check: fed.health_care_tax == 0 ? 'Yes' : 'Off',
      pct_11: number_to_currency(fed.health_care_tax),
      pct_12: number_to_currency(fed.tax_due + fed.health_care_tax)
    }

    # custom attributes
    attributes[:refund_13a] = number_to_currency(fed.compute < 0 ? -(fed.compute) : 0)
    attributes[:amount_you_owe] = number_to_currency(fed.compute > 0 ? fed.compute : 0)

    @pdf_data = {}
    PDF_FORM_FIELDS_MAPPING.each do |tz_attribute, pdf_field_name|
      @pdf_data[pdf_field_name] = attributes[tz_attribute]
    end
  end

  def generate_pdf
    @file_name = "#{@computation.id}_1040ez.pdf"
    @file_path = Rails.root.join('tmp', @file_name)
    @pdftk.fill_form PDF_PATH, @file_path, @pdf_data, flatten: true
  end
end
