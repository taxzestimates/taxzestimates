class Job::JobComputer

  JOB_VERSION = 0.2

  def initialize job
    @job = job
  end

  def federal_filing_status
    if recommended_federal_withholding <= 0
      nil
    else
      @job.person.filing_status
    end
  end

  def state_filing_status
    @job.person.filing_status
  end

  def federal_allowances
    if recommended_federal_withholding <= 0
      nil
    else
      10
    end
  end

  def state_allowances
    10
  end

  def recommended_federal_withholding
    amount = @job.federal_taxes_withheld_current + @job.proposed_federal_withholding_adjustment
    amount < 0 ? 0 : amount
  end

  def recommended_state_withholding
    amount = @job.state_taxes_withheld_current + @job.proposed_state_withholding_adjustment
    amount < 0 ? 0 : amount
  end

  # job version
  def get_version
    JOB_VERSION
  end
end
