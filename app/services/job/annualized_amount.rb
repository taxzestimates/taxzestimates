class Job::AnnualizedAmount
  def initialize job
    @job = job
  end

  def compute(amount)
    BigDecimal.new((amount / month) * 12)
  end

  def month
    @month ||= @job.date_of_pay_stub.to_i
  end
end
