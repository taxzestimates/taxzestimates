class BaseComputation::CAStateResult
  STANDARD_DEDUCTIONS = {
    single: 4044,
    married_filing_joint: 8088,
    married_filing_separate: 4044,
    head_of_household: 8088,
    surviving_spouse: 8088,
    dependent: 333,
    personal_exemption: 109,
    senior_exemption: 109
  }

  SINGLE_OR_MFS_BRACKET = [
    { min: 0,         max: 7_850,     tax_due: [0,          0.01] },
    { min: 7_850,     max: 18_610,    tax_due: [78.50,      0.02] },
    { min: 18_610,    max: 29_372,    tax_due: [293.70,     0.04] },
    { min: 29_372,    max: 40_773,    tax_due: [724.18,     0.06] },
    { min: 40_773,    max: 51_530,    tax_due: [1_408.24,   0.08] },
    { min: 51_530,    max: 263_222,   tax_due: [2_268.80,   0.093] },
    { min: 263_222,   max: 315_866,   tax_due: [21_956.16,  0.103] },
    { min: 315_866,   max: 526_443,   tax_due: [27_378.49,  0.113] },
    { min: 526_443,   max: nil,       tax_due: [51_173.69,  0.123] }
  ]

  MFJ_OR_SSP_BRACKET = [
    { min: 0,         max: 15_700,    tax_due: [0.00,       0.01] },
    { min: 15_700,    max: 37_220,    tax_due: [157.00,     0.02] },
    { min: 37_220,    max: 58_744,    tax_due: [587.40,     0.04] },
    { min: 58_744,    max: 81_546,    tax_due: [1_448.36,   0.06] },
    { min: 81_546,    max: 103_060,   tax_due: [2_816.48,   0.08] },
    { min: 103_060,   max: 526_444,   tax_due: [4_537.60,   0.093] },
    { min: 526_444,   max: 631_732,   tax_due: [43_912.31,  0.103] },
    { min: 631_732,   max: 1_052_886, tax_due: [54_756.97,  0.113] },
    { min: 1_052_886, max: nil,       tax_due: [102_347.37, 0.123] }
  ]

  TAX_BRACKETS = {
    single: SINGLE_OR_MFS_BRACKET,
    married_filing_joint: MFJ_OR_SSP_BRACKET,
    surviving_spouse: MFJ_OR_SSP_BRACKET,
    married_filing_separate: SINGLE_OR_MFS_BRACKET
  }

  RENT_CREDIT = {
    single: { credit: 60, agi_limit: 38_259 },
    married_filing_joint: { credit: 120, agi_limit: 76_518 }
  }

  def initialize computation
    @computation = computation
  end

  def compute
    tax_due - income_tax_withheld - rent_credit
  end

  def tax_due_before_exemptions
    if taxable_income > 0
      bracket = identify_bracket
      ((taxable_income - bracket[:min]) * bracket[:tax_due][1]) + bracket[:tax_due][0]
    else
      taxable_income
    end
  end

  def identify_bracket
    bracket = TAX_BRACKETS[@computation.filing_status.to_sym]
    selected_bracket = bracket.select do |b|
      (b[:min] <= taxable_income) &&
      ((b[:max] == nil) || (b[:max] >= taxable_income))
    end.try(:first) || 0
  end

  # Form 540 additions

  def senior_status
    ss = 0

    if @computation.is_senior?
      ss += 1
    end

    if @computation.is_filing_joint? and @computation.spouse.is_senior?
      ss += 11
    end

    ss == 0 ? nil : ss
  end

  def personal_status
    total = 0

    if @computation.is_filing_joint? and !@computation.spouse.claim_as_dependent
      total += 1
    end

    if !@computation.claim_as_dependent
      total += 1
    end

    total > 0 ? total : nil
  end

  def senior_exemptions
    senior_status ? senior_status * EXEMPTIONS[:senior] : nil
  end

  def personal_exemptions
    if personal_status
      personal_status * 109
    else
      nil
    end
  end

  def exemptions
    total_exemptions = 0

    if senior_exemptions
      total_exemptions += senior_exemptions
    end

    if personal_exemptions
      total_exemptions += personal_exemptions
    end

    total_exemptions == 0 ? nil : total_exemptions
  end

  def agi_after_subtractions
    if not @computation.is_filing_joint?
      if @computation.agi.present? and @computation.actual_unemployment_income.present?
        @computation.agi - @computation.actual_unemployment_income
      else
        0
      end
    else
      if @computation.joint_agi.present? and @computation.joint_actual_unemployment_income.present?
        @computation.joint_agi - @computation.joint_actual_unemployment_income
      else
        0
      end
    end
  end

  def state_agi
    agi_after_subtractions # use this for now since we have no additions
  end

  def standard_deduction
    BigDecimal(STANDARD_DEDUCTIONS[@computation.filing_status.to_sym])
  end

  def taxable_income
    result = state_agi - standard_deduction
    result > 0 ? result : 0
  end

  def tax_due
    if exemptions
      tax_due = tax_due_before_exemptions - exemptions
    else
      tax_due = tax_due_before_exemptions
    end

    tax_due > 0 ? tax_due : 0
  end

  def rent_credit
    bracket = RENT_CREDIT[@computation.filing_status.to_sym]

    if (not @computation.is_filing_joint? and (@computation.agi > bracket[:agi_limit] or (@computation.agi <= bracket[:agi_limit] and tax_due <= 0))) or
      (@computation.is_filing_joint? and (@computation.joint_agi > bracket[:agi_limit] or (@computation.joint_agi <= bracket[:agi_limit] and tax_due <= 0))) or
      (not @computation.is_filing_joint? and @computation.claim_as_dependent) or
      (@computation.is_filing_joint? and (@computation.claim_as_dependent or @computation.spouse.claim_as_dependent))
      0
    elsif (not @computation.is_filing_joint? and @computation.agi <= bracket[:agi_limit] and tax_due > 0 and tax_due < bracket[:credit]) or
      (@computation.is_filing_joint? and @computation.joint_agi <= bracket[:agi_limit] and tax_due > 0 and tax_due < bracket[:credit])
      tax_due
    else
      bracket[:credit]
    end
  end

  def income_tax_withheld
    if not @computation.is_filing_joint?
      @computation.total_state_withholdings
    else
      @computation.joint_total_state_withholdings
    end
  end

  def tax_after_credits
    tax_due - rent_credit
  end

  def state_refund
    income_tax_withheld - tax_after_credits
  end

  def state_liability
    tax_after_credits - income_tax_withheld
  end
end
