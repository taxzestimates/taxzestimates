class BaseComputation::FederalResult
  STANDARD_DEDUCTIONS_TABLE = {
    single: 6_300,
    married_filing_joint: 12_600,
    married_filing_separate: 6_300,
    head_of_household: 9_250,
    surviving_spouse: 12_600,
    dependent: 4_000
  }

  # TAX DUE ARRAY => [initial payment, percentage of taxable income over min - 1]
  INDIVIDUAL_BRACKET = [
    {min: 0, max: 9_225, tax_due: [0, 0.10]},
    {min: 9_226, max: 37_450, tax_due: [922.50, 0.15]},
    {min: 37_451, max: 90_750, tax_due: [5_156.25, 0.25]},
    {min: 90_751, max: 189_300, tax_due: [18_481.24, 0.28]},
    {min: 189_301, max: 411_500, tax_due: [46_075, 0.33]},
    {min: 411_501, max: 413_200, tax_due: [119_401, 0.35]},
    {min: 413_201, max: nil, tax_due: [119_996, 0.396]},
  ]

  MARRIED_FILING_JOINT_SURVIVING_BRACKET = [
    {min: 0, max: 18_450, tax_due: [0, 0.10]},
    {min: 18_451, max: 74_900, tax_due: [1_845, 0.15]},
    {min: 74_901, max: 151_200, tax_due: [10_313, 0.25]},
    {min: 151_201, max: 230_450, tax_due: [29_388, 0.28]},
    {min: 230_451, max: 411_500, tax_due: [51_578, 0.33]},
    {min: 411_501, max: 464_850, tax_due: [111_324, 0.35]},
    {min: 464_851, max: nil, tax_due: [129_997, 0.396]}
  ]

  MARRIED_FILING_SEPARATE_BRACKET = [
    {min: 0, max: 9_225, tax_due: [0, 0.10]},
    {min: 9_226, max: 37_450, tax_due: [992.50, 0.15]},
    {min: 37_451, max: 75_600, tax_due: [5_156.25, 0.25]},
    {min: 75_601, max: 115_225, tax_due: [14_963.75, 0.28]},
    {min: 115_226, max: 205_750, tax_due: [25_788.75, 0.33]},
    {min: 205_751, max: 232_425, tax_due: [55_662, 0.35]},
    {min: 232_426, max: nil, tax_due: [64_998.25, 0.396]}
  ]

  HEAD_OF_HOUSEHOLD_BRACKET = [
    {min: 0, max: 13_150, tax_due: [0, 0.10]},
    {min: 13_151, max: 50_200, tax_due: [1_315, 0.15]},
    {min: 50_201, max: 129_600, tax_due: [6_872.50, 0.25]},
    {min: 129_601, max: 209_850, tax_due: [26_772.50, 0.28]},
    {min: 209_851, max: 411_500, tax_due: [49_192.50, 0.33]},
    {min: 411_501, max: 439_000, tax_due: [115_737, 0.35]},
    {min: 439_001, max: nil, tax_due: [125_362, 0.396]}
  ]

  TAX_BRACKETS = {
    single: INDIVIDUAL_BRACKET,
    married_filing_joint: MARRIED_FILING_JOINT_SURVIVING_BRACKET,
    surviving_spouse: MARRIED_FILING_JOINT_SURVIVING_BRACKET,
    married_filing_separate: MARRIED_FILING_SEPARATE_BRACKET,
    head_of_household: HEAD_OF_HOUSEHOLD_BRACKET
  }

  HEALTH_CARE = {
    monthly_fee: 27.08,
    taxable_income_multi: 0.02,
    max_months_no_insurance: 3
  }

  EARNED_INCOME_CREDIT = {
    amount: 6_580,
    max_credit: 503,
    age_limit: [25, 65],
    single_threshold_phaseout_start: 8_240,
    single_threshold_phaseout_end: 14_820,
    joint_threshold_phaseout_start: 13_760,
    joint_threshold_phaseout_end: 20_340
  }

  def initialize computation
    @computation = computation
  end

  def compute
    tax_after_credits - income_tax_withheld
  end

  def tax_after_credits
    total_tax_due - earned_income_credit
  end

  def total_tax_due
    tax_due + health_care_tax
  end

  def tax_due
    if taxable_income > 0
      bracket = identify_bracket
      ((taxable_income - bracket[:min]) * bracket[:tax_due][1]) + bracket[:tax_due][0]
    else
      taxable_income
    end
  end

  def identify_bracket
    bracket = TAX_BRACKETS[@computation.filing_status.to_sym]
    selected_bracket = bracket.select do |b|
      (b[:min] <= taxable_income) &&
      ((b[:max] == nil) || (b[:max] >= taxable_income))
    end.try(:first) || 0
  end

  def taxable_income
    result = @computation.joint_agi - standard_deduction - personal_deduction
    result > 0 ? result : 0
  end

  def standard_deduction
    BigDecimal(STANDARD_DEDUCTIONS_TABLE[@computation.filing_status.to_sym])
  end

  def personal_deduction
    current_deduction = 0

    personal_deduction_amount = STANDARD_DEDUCTIONS_TABLE[:dependent]

    if !@computation.claim_as_dependent
      current_deduction += personal_deduction_amount
    end

    if @computation.is_filing_joint? && !@computation.spouse.claim_as_dependent
      current_deduction += personal_deduction_amount
    end

    current_deduction
  end

  # spouse formulae
  def spouse_taxable_income
    result = @computation.spouse.agi - standard_deduction - personal_deduction
    result > 0 ? result : 0
  end
  # end spouse formulae

  # joint formulae
  def joint_taxable_income
    if @computation.is_filing_joint?
      result = @computation.agi.to_s.to_d + @computation.spouse.agi.to_s.to_d - standard_deduction - personal_deduction
    else
      result = @computation.agi.to_s.to_d - standard_deduction - personal_deduction
    end

    result > 0 ? result : 0
  end
  # end joint formulae

  def health_care_tax
    if not @computation.is_filing_joint?
      if @computation.has_health_insurance or
        @computation.agi < (standard_deduction + personal_deduction) or
        (not @computation.has_health_insurance and @computation.months_without_health_insurance <= HEALTH_CARE[:max_months_no_insurance])
        0
      else
        if (@computation.months_without_health_insurance * HEALTH_CARE[:monthly_fee]) > (taxable_income * HEALTH_CARE[:taxable_income_multi])
          @computation.months_without_health_insurance * HEALTH_CARE[:monthly_fee]
        else
          taxable_income * HEALTH_CARE[:taxable_income_multi]
        end
      end
    else
      if (@computation.has_health_insurance and @computation.spouse.has_health_insurance) or
        (@computation.joint_agi < (standard_deduction + personal_deduction)) or
        (not @computation.has_health_insurance and not @computation.spouse.has_health_insurance and @computation.months_without_health_insurance <= HEALTH_CARE[:max_months_no_insurance] and @computation.spouse.months_without_health_insurance <= HEALTH_CARE[:max_months_no_insurance]) or
        (@computation.has_health_insurance and not @computation.spouse.has_health_insurance and @computation.spouse.months_without_health_insurance <= HEALTH_CARE[:max_months_no_insurance]) or
        (not @computation.has_health_insurance and @computation.spouse.has_health_insurance and @computation.months_without_health_insurance <= HEALTH_CARE[:max_months_no_insurance])
        0
      elsif (@computation.has_health_insurance and not @computation.spouse.has_health_insurance and @computation.spouse.months_without_health_insurance > HEALTH_CARE[:max_months_no_insurance]) or
        (not @computation.has_health_insurance and not @computation.spouse.has_health_insurance and @computation.months_without_health_insurance <= HEALTH_CARE[:max_months_no_insurance] and @computation.spouse.months_without_health_insurance > HEALTH_CARE[:max_months_no_insurance])
        if (@computation.spouse.months_without_health_insurance * HEALTH_CARE[:monthly_fee]) > (taxable_income * HEALTH_CARE[:taxable_income_multi])
          @computation.spouse.months_without_health_insurance * HEALTH_CARE[:monthly_fee]
        else
          taxable_income * HEALTH_CARE[:taxable_income_multi]
        end
      elsif (not @computation.has_health_insurance and @computation.spouse.has_health_insurance and @computation.months_without_health_insurance > HEALTH_CARE[:max_months_no_insurance]) or
        (not @computation.has_health_insurance and not @computation.spouse.has_health_insurance and @computation.months_without_health_insurance > HEALTH_CARE[:max_months_no_insurance] and @computation.spouse.months_without_health_insurance <= HEALTH_CARE[:max_months_no_insurance])
        if (@computation.months_without_health_insurance * HEALTH_CARE[:monthly_fee]) > (taxable_income * HEALTH_CARE[:taxable_income_multi])
          @computation.months_without_health_insurance * HEALTH_CARE[:monthly_fee]
        else
          taxable_income * HEALTH_CARE[:taxable_income_multi]
        end
      elsif (not @computation.has_health_insurance and not @computation.spouse.has_health_insurance and @computation.months_without_health_insurance > HEALTH_CARE[:max_months_no_insurance] and @computation.spouse.months_without_health_insurance > HEALTH_CARE[:max_months_no_insurance])
        if ((@computation.months_without_health_insurance + @computation.spouse.months_without_health_insurance) * HEALTH_CARE[:monthly_fee]) > (taxable_income * HEALTH_CARE[:taxable_income_multi])
          (@computation.months_without_health_insurance + @computation.spouse.months_without_health_insurance) * HEALTH_CARE[:monthly_fee]
        else
          taxable_income * HEALTH_CARE[:taxable_income_multi]
        end
      end
    end 
  end

  def earned_income_credit
    if not @computation.is_filing_joint?
      if @computation.agi < EARNED_INCOME_CREDIT[:amount] or
        @computation.agi >= EARNED_INCOME_CREDIT[:single_threshold_phaseout_end] or
        @computation.age < EARNED_INCOME_CREDIT[:age_limit][0] or
        @computation.age > EARNED_INCOME_CREDIT[:age_limit][1] or
        @computation.claim_as_dependent
        0
      elsif @computation.agi >= EARNED_INCOME_CREDIT[:amount] and @computation.agi <= EARNED_INCOME_CREDIT[:single_threshold_phaseout_start]
        EARNED_INCOME_CREDIT[:max_credit]
      elsif @computation.agi > EARNED_INCOME_CREDIT[:single_threshold_phaseout_start] and @computation.agi < EARNED_INCOME_CREDIT[:single_threshold_phaseout_end]
        EARNED_INCOME_CREDIT[:max_credit] - (@computation.agi - EARNED_INCOME_CREDIT[:single_threshold_phaseout_start]) / (EARNED_INCOME_CREDIT[:single_threshold_phaseout_end] - EARNED_INCOME_CREDIT[:single_threshold_phaseout_start]) * EARNED_INCOME_CREDIT[:max_credit]
      elsif @computation.agi > EARNED_INCOME_CREDIT[:amount] and @computation.agi < EARNED_INCOME_CREDIT[:single_threshold_phaseout_start]
        EARNED_INCOME_CREDIT[:max_credit]
      end
    else
      if @computation.joint_agi < EARNED_INCOME_CREDIT[:amount] or
        @computation.joint_agi > EARNED_INCOME_CREDIT[:joint_threshold_phaseout_end] or
        @computation.age < EARNED_INCOME_CREDIT[:age_limit][0] or
        @computation.age > EARNED_INCOME_CREDIT[:age_limit][1] or
        @computation.claim_as_dependent or
        @computation.spouse.age < EARNED_INCOME_CREDIT[:age_limit][0] or
        @computation.spouse.age > EARNED_INCOME_CREDIT[:age_limit][1] or
        @computation.spouse.claim_as_dependent
        0
      elsif @computation.joint_agi >= EARNED_INCOME_CREDIT[:joint_threshold_phaseout_start] and @computation.joint_agi <= EARNED_INCOME_CREDIT[:joint_threshold_phaseout_end]
        EARNED_INCOME_CREDIT[:max_credit] - (@computation.joint_agi - EARNED_INCOME_CREDIT[:joint_threshold_phaseout_start]) / (EARNED_INCOME_CREDIT[:joint_threshold_phaseout_end] - EARNED_INCOME_CREDIT[:joint_threshold_phaseout_start]) * EARNED_INCOME_CREDIT[:max_credit]
      elsif @computation.joint_agi >= EARNED_INCOME_CREDIT[:amount] and @computation.joint_agi <= EARNED_INCOME_CREDIT[:joint_threshold_phaseout_start]
        EARNED_INCOME_CREDIT[:max_credit]
      end
    end
  end

  def tax_after_credits
    # use only earned income credit for now
    after_credits = tax_due - earned_income_credit
  end

  def income_tax_withheld
    if not @computation.is_filing_joint?
      @computation.total_federal_withholdings
    else
      @computation.joint_total_federal_withholdings
    end
  end
end
