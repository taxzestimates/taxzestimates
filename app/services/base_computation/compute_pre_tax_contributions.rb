class BaseComputation::ComputePreTaxContributions

  DURATION = {
    MONTHLY: 0,
    YEARLY: 1
  }

  CONTRIBUTIONS = [
    'mass_transportation', 'mass_parking_investment'#, 'retirement_account'
  ]

  YEARLY_MAX_DEDUCTIONS = {
    mass_transportation: 1_560,
    mass_parking_investment: 3_000,
    retirement_account: 5_500
  }

  DURATION = {
    MONTHLY: 0,
    YEARLY: 1
  }

  def initialize computation
    @computation = computation
  end

  def compute_annual
    CONTRIBUTIONS.reduce(0) do |sum, contribution|
      sum_annualized = 0

      for job in @computation.jobs
        amount = job.send(contribution)
        annualized_amount = Job::AnnualizedAmount.new(job).compute(amount)

        sum_annualized += annualized_amount
      end

      max_deduction =
        if contribution == 'retirement_account'
          @computation.age < 50 ? 5_500 : 6_500
        else
          YEARLY_MAX_DEDUCTIONS[contribution.to_sym]
        end

      annualized_deduction =
        if sum_annualized > max_deduction
          max_deduction
        else
          sum_annualized
        end

      sum + annualized_deduction
    end
  end



  # def contributions
  #   @contributions ||= @computation.hash_attrs.slice('mass_transportation', 'mass_parking_investment', 'retirement_account')
  # end

end
