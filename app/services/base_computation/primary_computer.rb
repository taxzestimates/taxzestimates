class BaseComputation::PrimaryComputer
  include ActionView::Helpers::NumberHelper

  COMPUTATION_VERSION = 0.8
  
  def initialize computation
    @computation = computation
  end

  def agi
    @computation.wages_salaries + actual_interest_income + @computation.actual_unemployment_income
  end

  def federal_result
    BaseComputation::FederalResult.new(@computation).compute
  end

  def state_result
    BaseComputation::CAStateResult.new(@computation).compute
  end

  def federal_credits
    fed = BaseComputation::FederalResult.new(@computation)

    fed_credits = []
    fed_credits << {text: 'Earned Income Credit', amount: number_to_currency(BigDecimal(fed.earned_income_credit), :precision => 2) }
    fed_credits.to_json
  end

  def state_credits #california
    state = BaseComputation::CAStateResult.new(@computation)

    state_credits = []
    state_credits << {text: 'Rent Credit', amount: number_to_currency(BigDecimal(state.rent_credit), :precision => 2) }
    state_credits.to_json
  end

  def taxable_income
    BaseComputation::FederalResult.new(@computation).joint_taxable_income
  end

  def actual_interest_income
    if @computation.has_interest_income
      @computation.interest_income
    else
      0
    end
  end

  def joint_interest_income
    if @computation.is_filing_joint?
      actual_interest_income + @computation.spouse.actual_interest_income
    else
      actual_interest_income
    end
  end

  # dashboard
  def federal_tax_rate
    if @computation.joint_agi > 0
      BaseComputation::FederalResult.new(@computation).tax_after_credits / @computation.joint_agi
    else
      0
    end
  end

  def state_tax_rate
    if @computation.joint_wages_salaries > 0
      BaseComputation::CAStateResult.new(@computation).tax_after_credits / @computation.joint_wages_salaries
    else
      0
    end
  end

  def effective_tax_rate
    if @computation.joint_wages_salaries > 0
      (BaseComputation::FederalResult.new(@computation).tax_after_credits + BaseComputation::CAStateResult.new(@computation).tax_after_credits) / @computation.joint_wages_salaries
    else
      0
    end
  end

  def federal_taxes
    BaseComputation::FederalResult.new(@computation).tax_after_credits
  end

  def state_taxes
    BaseComputation::CAStateResult.new(@computation).tax_after_credits
  end

  def take_home_income
    @computation.joint_agi - federal_taxes - state_taxes
  end

  def latest_paycheck
    month = nil

    if @computation.jobs.length > 0
      month = @computation.jobs.last.date_of_pay_stub
    elsif @computation.is_filing_joint?
      if @computation.spouse.jobs.length > 0
        month = @computation.spouse.jobs.last.date_of_pay_stub
      end
    end

    month
  end

  # computation version
  def get_version
    COMPUTATION_VERSION
  end
end
