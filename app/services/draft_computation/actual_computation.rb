class DraftComputation::ActualComputation
  def initialize draft_computation
    @draft_computation = draft_computation
    @user = @draft_computation.user
  end

  def generate
    computation = @user.computations.create(computation_attributes)
    #computation.dependents.create(dependent_attributes) if @draft_computation.dependents.present?
    computation.jobs.create(job_attributes) if @draft_computation.jobs.present?

    if computation.filing_status == 'married_filing_joint'
      computation.create_spouse(spouse_attributes) if @draft_computation.spouse.present?
      computation.spouse.jobs.create(spouse_job_attributes) if @draft_computation.spouse.jobs.present?
    end

    computation.reload
  end

  def raw_attributes
    Hash[@draft_computation.attributes].with_indifferent_access
  end

  def computation_attributes
    raw_attributes.except(:current_step, :_id, :dependents, :spouse, :jobs, :_type, :created_at, :updated_at)
  end

  def spouse_attributes
    raw_attributes[:spouse].except(:_id, :jobs)
  end

  def dependent_attributes
    raw_attributes[:dependents].map do |dependent|
      dependent.except(:_id)
    end
  end

  def job_attributes
    raw_attributes[:jobs].map do |job|
      job.except(:_id)
    end
  end

  def spouse_job_attributes
    raw_attributes[:spouse][:jobs].map do |job|
      job.except(:_id)
    end
  end
end
