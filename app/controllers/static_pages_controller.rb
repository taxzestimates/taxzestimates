class StaticPagesController < ApplicationController
  layout 'website/application'

  def index
    if user_signed_in?
      redirect_to :tax
    end
  end

  def about
  end

  def contact
  end

  def privacy_policy
  end

  def terms_and_conditions
  end

  def robots
    robot_type = Rails.env == "production" ? "production" : "staging"
    robots = File.read(Rails.root + "config/robots/robots.#{robot_type}.txt")
    render :text => robots, :layout => false, :content_type => "text/plain"
  end

  def funnel
    render layout: false
  end
end
