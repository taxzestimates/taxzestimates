class WebsiteController < ApplicationController
  layout 'website/application'

  def home
    if user_signed_in?
      redirect_to :tax
    end
  end
end
