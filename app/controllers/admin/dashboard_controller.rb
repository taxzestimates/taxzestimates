# app/controllers/admin/dashboard_controller.rb
class Admin::DashboardController < Admin::BaseController
	include ActionView::Helpers::NumberHelper

	def index
		@total_users = User.count
		@active_users = User.where("deactivated_at IS NULL").count
		@inactive_users = @total_users - @active_users

		# user growth
		users_before = User.where("DATE(created_at) = ?", Date.today - 1).count
		users_now = User.where("DATE(created_at) = ?", Date.today).count

		@user_growth = user_growth(users_before, users_now)

		user_totals = get_user_totals('12-mos')
		@labels, @data = get_metric(user_totals, 'total-users', '12-mos')
	end

	def user_growth(before, now)
		if before == 0
			if now == 0
				0
			else
				100
			end
		elsif before > now
			0
		else
			((now - before) / before) * 100
		end
	end

	def graph_info
		user_totals = get_user_totals(params[:period])
		labels, data = get_metric(user_totals, params[:metric], params[:period])

		case params[:metric]
			when "total-users"
				title = "Total Users"
			when "user-growth"
				title = "User Growth"
			end

		respond_to do |format|
		  format.json do
		  	render json: {
		  		title: title,
		  		labels: labels,
		  		data: data,
		  		success: true
		  	}.to_json
		  end
		end
	end

	def get_user_totals(period)
		case period
		when "12-mos"
			User.group_by_month(:created_at, format: "%Y-%m-%d", last: 12).count
		when "7-days"
			User.group_by_day(:created_at, format: "%Y-%m-%d", last: 7).count
		end
	end

	def get_previous_total(period)
		case period
		when "12-mos"
			User.where('extract(month from created_at) = ? and extract(year from created_at) = ?', 13.months.ago.month, 13.months.ago.year).count
		when "7-days"
			User.where('extract(day from created_at) = ?', 8.days.ago.day).count
		end
	end

	def get_metric(user_totals, metric, period)
		labels = Array.new
		data = Array.new
		current_year = nil
		previous_total = get_previous_total(period)

		user_totals.each do |row|
			date = DateTime.parse(row[0])
			year = date.strftime("%Y")

			case period
			when "12-mos"
				if current_year != year
					labels.push(date.strftime("%b %Y"))
					current_year = year
				else
					labels.push(date.strftime("%b"))
				end
			when "7-days"
				labels.push(date.strftime("%b %e"))
			end

			case metric
			when "total-users"
				data.push(row[1])
			when "user-growth"
				data.push(user_growth(previous_total, row[1]))
			end

			previous_total = row[1]
		end

		return labels, data
	end
end