# app/controllers/admin/user_controller.rb
class Admin::UserController < Admin::BaseController
  helper_method :sort_column, :sort_direction

	def index
=begin
    @filing_status = {
      'single' => "Single",
      'married_filing_joint' => "Married Filing Joint"
    }

    @users = BaseComputation.collection.aggregate(
      [
        {
          "$group" => {
            _id: "$user_id",
            id: { "$last" => "$_id" },
            first_name: { "$last" => "$first_name" },
            last_name: { "$last" => "$last_name" },
            mi: { "$last" => "$mi" },
            birth_month: { "$last" => "$birth_month" },
            birth_day: { "$last" => "$birth_day" },
            birth_year: { "$last" => "$birth_year" },
            filing_status: { "$last" => "$filing_status" },
            city: { "$last" => "$city" },
            state: { "$last" => "$state" },
            user_id: { "$last" => "$user_id" }
          }
        },
        {
          "$sort" => { sort_column => sort_direction == "asc" ? 1 : -1 }
        }
      ]
    )

    @parents = {}

    @users.each do |u|
      parent = User.where(:id => u['user_id'])

      if not parent.blank?
        @parents[u['id'].to_s] = parent.to_a[0]
      end
    end
=end
    @users = User.search(params[:search]).order("deactivated_at desc, " + sort_column + " " + sort_direction).paginate(:per_page => params[:per_page], :page => params[:page])

    respond_to do |format|
      format.html
      format.csv { send_data @users.to_csv, filename: "users-#{Date.today}.csv" }
    end
	end

  def sort_column
    User.column_names.include?(params[:sort]) ? params[:sort] : "last_name"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def toggle
    if params[:toggle] == 'deactivate'
      User.find(params[:id]).soft_delete
    else
      User.find(params[:id]).reactivate_account
    end

    flash[:type] = "success"
    flash[:notice] = "User has been " + params[:toggle] + "d."

    respond_to do |format|
      format.json do
        render json: {
          success: true
        }.to_json
      end
    end
  end
end