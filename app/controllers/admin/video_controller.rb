# app/controllers/admin/video_controller.rb
class Admin::VideoController < Admin::BaseController
  helper_method :sort_column, :sort_direction

	def index
    @videos = Video.search(params[:search]).order("\"primary\" desc, active desc, " + sort_column + " " + sort_direction).paginate(:per_page => params[:per_page], :page => params[:page])
	end

  def sort_column
    Video.column_names.include?(params[:sort]) ? params[:sort] : "title"
  end
  
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

  def toggle
    if params[:toggle] == 'deactivate'
      Video.find(params[:id]).deactivate
    else
      Video.find(params[:id]).reactivate
    end

    flash[:type] = "success"
    flash[:notice] = "Video has been " + params[:toggle] + "d."

    respond_to do |format|
      format.json do
        render json: {
          success: true
        }.to_json
      end
    end
  end

  def set_primary
    Video.find(params[:id]).set_primary

    flash[:type] = "success"
    flash[:notice] = "Primary video has been set."

    respond_to do |format|
      format.json do
        render json: {
          success: true
        }.to_json
      end
    end
  end

  def create
    data = {
      :title => params[:title],
      :url => params[:url],
      :description => params[:description],
      :active => true
    }

    Video.create(data)

    flash[:type] = "success"
    flash[:notice] = "Video has been added."

    redirect_to admin_video_index_path
  end

  def update

  end

  def destroy
    Video.destroy(params[:id])

    flash[:type] = "success"
    flash[:notice] = "Video has been deleted."

    respond_to do |format|
      format.json do
        render json: {
          success: true
        }.to_json
      end
    end
  end
end