# app/controllers/admin/base_controller.rb
class Admin::BaseController < ApplicationController
	layout 'layouts/admin/application'
	before_filter :login_required
	before_filter :is_admin?
	before_filter :set_pagedata

	def set_pagedata
		 @user = current_user
	end

	def is_admin?
	  if current_user.admin?
	    true
	  else
	    render :status => :forbidden, :text => "Unauthorized"
	  end
	end
end