class VideoController < ApplicationController
  before_filter :login_required

  def index
  	@user = current_user

    @latest = @user.computations.last
    @draft = @user.draft_computation

    @latest = @latest.present? ? ComputationPresenter.new(@latest) : {}
    @draft = @draft.present? ? ComputationPresenter.new(@draft) : {}
    
    render layout: 'layouts/user/application'
  end
end

