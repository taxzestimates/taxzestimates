# OVERRIDE DEVISE REGISTRATIONS CONTROLLER

class RegistrationsController < Devise::RegistrationsController
  respond_to :json

  before_filter :configure_permitted_parameters
  before_filter :login_required, :except => [:create]

  def create
    respond_to do |format|
      build_resource(sign_up_params)

      resource.save
      yield resource if block_given?
      if resource.persisted?
        if resource.active_for_authentication?
          flash_key = 'signed_up'
          sign_up(resource_name, resource)
        else
          flash_key = 'signed_up_but_unconfirmed'
          expire_data_after_sign_in!
        end

        response = { 'success' => I18n.t('devise.registrations.' + flash_key) }

        format.json { render :json => response }
      else
        format.json { render :json => resource.errors.full_messages.uniq }
      end
    end
  end

  def update
    respond_to do |format|
      account_update_params = devise_parameter_sanitizer.sanitize(:account_update)
      prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

      # required for settings form to submit when password is left blank
      if account_update_params[:password].blank?
        account_update_params.delete("password")
        account_update_params.delete("password_confirmation")
      end

      @user = User.find(current_user.id)

      if update_resource(@user, account_update_params)
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          'update_needs_confirmation' : 'updated'

        response = { 'success' => I18n.t('devise.registrations.' + flash_key), 'user' => @user }

        sign_in resource_name, resource, bypass: true

        format.json { render :json => response }
      else
        format.json { render :json => @user.errors.full_messages.uniq }
        #format.html { render :nothing => true }
      end
    end
  end

  # deactivate account
  def destroy
    if resource.valid_password?(params[:user][:current_password])
      current_user.update_attribute(:deactivation_reason, params[:user][:deactivation_reason])
      resource.soft_delete
      Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
      #set_flash_message :notice, :destroyed if is_flashing_format?
      #yield resource if block_given?
      #respond_with_navigational(resource){ redirect_to after_sign_out_path_for(resource_name) }
      render :json => { :success => I18n.t('devise.registrations.destroyed') }
    else
      render :json => { :error => I18n.t('devise.failure.wrong_password') }
    end
  end

  def avatar
    file = params[:avatar].is_a?(ActionDispatch::Http::UploadedFile) ? params[:avatar] : params[:file]
    @user = current_user
    if @user.update_attributes({ :avatar => file })
      render :json => { :success => true, "img" => { "thumb" => @user.avatar.url(:thumb), "medium" => @user.avatar.url(:medium) }}
    else
      render :json => { :success => false }
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:email, :first_name, :last_name, :password, :password_confirmation)
    end
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:email, :first_name, :last_name, :password, :password_confirmation, :current_password, :avatar, :deactivation_reason)
    end
  end

  def update_resource(resource, params)
    if resource.email != params[:email] || params[:password].present?
      super
    else
      params.delete(:current_password)
      resource.update_without_password(params)
    end
  end

  # Sets minimum password length to show to user
  def set_minimum_password_length
    if devise_mapping.validatable?
      @minimum_password_length = resource_class.password_length.min
    end
  end
end