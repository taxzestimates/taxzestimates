class Api::InvitationsController < Api::ApplicationController
  def save_invite
    invite = Invite.create(email: params[:email], ip_address: request.remote_ip)
    @resp[:success] = invite.persisted?
    @resp[:errors] = invite.errors.full_messages
    render json: @resp
  end
end
