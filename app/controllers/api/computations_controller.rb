class Api::ComputationsController < Api::ApplicationController
  before_filter :login_required

  def index
    result = current_user.computations.order_by([:created_at, :desc]).entries.map do |c|
      ComputationPresenter.new(c)
    end

    render json: result
  end

  def show
    @computation = current_user.computations.find(params[:id])
    respond_to do |format|
      format.html { render json: ComputationPresenter.new(@computation) }
      format.json { render json: ComputationPresenter.new(@computation) }
      format.pdf { render pdf: "computation-#{@computation.id}" }
    end
  end

  def form1040ez
    @computation = current_user.computations.find(params[:id])
    generator = PDFGenerator::Form1040ez.new(@computation)
    send_file generator.file_path, filename: generator.file_name, type: 'application/pdf', disposition: 'inline'
  end

  def form540ez
    @computation = current_user.computations.find(params[:id])
    generator = PDFGenerator::Form540ez.new(@computation)
    send_file generator.file_path, filename: generator.file_name, type: 'application/pdf', disposition: 'inline'
  end

  def latest
    render json: current_user.computations.last.to_json
  end

  private
    def permitted_computation_params
      params.require(:computation).permit(
        :first_name,
        :last_name,
        :mi,
        :birth_month,
        :birth_day,
        :birth_year,
        :address_1,
        :address_2,
        :city,
        :state,
        :zip,
        :social_security_number,
        :income_type,
        :filing_status,
        #:jobs_count,

        :children_dependents,
        :relatives_dependents,

        :number_of_allowances,
        :retirement_account,

        :claim_as_dependent,
        :has_health_insurance,
        :months_without_health_insurance,

        :has_interest_income,
        :interest_income,
        :has_unemployment_income,
        :is_receiving_unemployment_income,
        :unemployment_income_received_date,
        :unemployment_income,
        :unemployment_income_bi_weekly
      )
    end

    def permitted_spouse_params
      params.permit(spouse: [
        :first_name,
        :last_name,
        :mi,
        :birth_month,
        :birth_day,
        :birth_year,
        :address_1,
        :address_2,
        :city,
        :state,
        :zip,
        :social_security_number,
        :income_type,
        #:jobs_count,

        :number_of_allowances,
        :retirement_account,

        :claim_as_dependent,
        :has_health_insurance,
        :months_without_health_insurance,

        :federal_taxes_withheld_current,
        :state_taxes_withheld_current,

        :has_interest_income,
        :interest_income,
        :has_unemployment_income,
        :is_receiving_unemployment_income,
        :unemployment_income_received_date,
        :unemployment_income,
        :unemployment_income_bi_weekly
      ])
    end

    def permitted_dependent_params
      params.permit(dependents: [:dependent_type,
        :first_name,
        :last_name,
        :mi,
        :birth_day,
        :birth_year,
        :birth_month,
        :social_security_number,
      ])
    end

    def permitted_job_params
      params.permit(jobs: [
        :company_name,
        :is_currently_employed,

        :date_of_pay_stub,
        :paycheck_frequency,
        :salary_current,
        :salary,
        :bonus,
        
        :mass_transportation,
        :mass_transportation_current,
        :mass_parking_investment,
        :mass_parking_investment_current,

        :federal_taxes_withheld_current,
        :state_taxes_withheld_current,

        :claim_as_dependent,
        :has_health_insurance,
        :months_without_health_insurance,

        :federal_taxes_withheld,
        :state_taxes_withheld
      ])
    end

    def permitted_spouse_job_params
      params.permit(spouse: {
        jobs: [
          :company_name,
          :is_currently_employed,

          :date_of_pay_stub,
          :paycheck_frequency,
          :salary_current,
          :salary,
          :bonus,
          
          :mass_transportation,
          :mass_transportation_current,
          :mass_parking_investment,
          :mass_parking_investment_current,

          :federal_taxes_withheld_current,
          :state_taxes_withheld_current,

          :claim_as_dependent,
          :has_health_insurance,
          :months_without_health_insurance,

          :federal_taxes_withheld,
          :state_taxes_withheld,
        ]
      })
    end
end
