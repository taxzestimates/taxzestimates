class Api::DraftComputationsController < Api::ComputationsController
  def create
    computation_params = permitted_computation_params

    computation = current_user.create_draft_computation(computation_params)

    if not computation.valid?
      render :json => { :errors => computation.errors.full_messages }, :status => 400 and return
    end

    # Spouse
    spouse_params = permitted_spouse_params[:spouse]
    if spouse_params.present?
      computation.create_spouse(spouse_params)
    end

    # Dependents
    dependent_params = permitted_dependent_params[:dependents]
    if dependent_params.present?
      computation.dependents.create(dependent_params)
    end

    # Jobs
    job_params = permitted_job_params[:jobs]
    if job_params.present?
      computation.jobs.create(job_params)
    end

    # Spouse Jobs
    spouse_job_params = permitted_spouse_job_params[:jobs]
    if spouse_job_params.present?
      computation.spouse.jobs.create(spouse_job_params)
    end

    render json: ComputationPresenter.new(computation)
  end

  def update
    computation = current_user.draft_computation

    if computation.present?
      # User was previously Single and changed status to Married, so make him/her repeat the succeeding forms.
      if computation.filing_status == 'single' and params[:draft_computation][:filing_status] == 'married_filing_joint'
        params[:draft_computation][:current_step] = 3
      end
      
      computation.update_attributes(permitted_computation_params)

      if not computation.valid?
        render :json => { :errors => computation.errors.full_messages }, :status => 400 and return
      end
    end

    computation.spouse.try(:destroy)

    if params[:draft_computation][:filing_status] == 'married_filing_joint'
      # Always destroy and recreate spouse and dependents object if present
      # Spouse
      spouse_params = permitted_spouse_params[:spouse]
      if spouse_params.present?
        computation.create_spouse(spouse_params)

        if not computation.spouse.valid?
          render :json => { :errors => computation.spouse.errors.full_messages }, :status => 400 and return
        end
      end

      spouse_job_params = permitted_spouse_job_params[:spouse][:jobs]
      if spouse_job_params.present?
        computation.spouse.jobs.create(spouse_job_params)

        computation.spouse.jobs.each do |job|
          if not job.valid?
            render :json => { :errors => job.errors.full_messages }, :status => 400 and return
          end
        end
      end
    end

    # Dependents
    #dependent_params = permitted_dependent_params[:dependents]
    #if dependent_params.present?
    #  computation.dependents.destroy
    #  computation.dependents.create(dependent_params)
    #end

    job_params = permitted_job_params[:jobs]

    computation.jobs.destroy

    if job_params.present?
      computation.jobs.create(job_params)

      computation.jobs.each do |job|
        if not job.valid?
          render :json => { :errors => job.errors.full_messages }, :status => 400 and return
        end
      end
    end

    # recompute all saved values
    computation.recompute

    render json: ComputationPresenter.new(computation)
  end

  def generate_actual_computation
    draft = current_user.draft_computation
    computation = draft.generate_actual_computation
    render json: ComputationPresenter.new(computation)
  end

  private
    def permitted_computation_params
      params.require(:draft_computation).permit(
        :first_name,
        :last_name,
        :mi,
        :birth_month,
        :birth_day,
        :birth_year,
        :address_1,
        :address_2,
        :city,
        :state,
        :zip,
        :social_security_number,
        :income_type,
        :filing_status,
        #:jobs_count,

        :number_of_allowances,
        :retirement_account,

        :children_dependents,
        :relatives_dependents,

        :claim_as_dependent,
        :has_health_insurance,
        :months_without_health_insurance,

        :has_interest_income,
        :interest_income,
        :has_unemployment_income,
        :is_receiving_unemployment_income,
        :unemployment_income_received_date,
        :unemployment_income,
        :unemployment_income_bi_weekly,

        :current_step
      )
    end
end
