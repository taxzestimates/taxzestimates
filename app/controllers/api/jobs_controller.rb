class Api::JobsController < Api::ApplicationController
  before_filter :login_required

  def w4
    @job = 
      if request.fullpath.include? "/spouse/"
        current_user.computations.find(params[:computation_id]).spouse.jobs.find(params[:job_id])
      else
        current_user.computations.find(params[:computation_id]).jobs.find(params[:job_id])
      end

    w4 = PDFGenerator::W4.new(@job)
    send_file w4.file_path, filename: w4.file_name, type: 'application/pdf', disposition: 'inline'
  end

  def stateW4
    @job = 
      if request.fullpath.include? "/spouse/"
        current_user.computations.find(params[:computation_id]).spouse.jobs.find(params[:job_id])
      else
        current_user.computations.find(params[:computation_id]).jobs.find(params[:job_id])
      end

    w4 = PDFGenerator::StateW4.new(@job)
    send_file w4.file_path, filename: w4.file_name, type: 'application/pdf', disposition: 'inline'
  end
end