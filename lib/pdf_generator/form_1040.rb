class PdfGenerator::Form1040 < PdfGenerator::Base
  FILENAME = '1040ez'
  def generate_ghost_document
    RGhost::Config::GS[:unit]=RGhost::Units::Cm
    rGhostDocument = RGhost::Document.new paper: [26.924,34.29]#[widthxheight]
    rGhostDocument.define_tags do |t|
      t.tag :default_font, size: 14
      t.tag :small_font, size: 12
    end
    rGhostDocument.first_page do |doc|
      doc.image "#{TEMPLATE_PATH}/#{FILENAME}.eps"
      doc.text_in x: 5.95, y: 29.86, text: "THIS IS A TEST!!!!"
      # doc.text_in x: 5.95, y: 29.86, text: "#{employee.name}"
      # doc.text_in x: 17.85, y: 29.86, text: "#{employee.ssn}"
      # doc.text_in x: 11.4, y: 25.8, text: "#{employee.routing_number}"
      # doc.text_in x: 7, y: 23.8, text: "#{employee.account_number}"
      # doc.text_in x: 10.9, y: 22.25, text: get_account_type(employee.account_type)
      # doc.text_in x: 15.7, y:13.1, text: "#{employee.created_at.strftime('%m/%d/%Y')}"
    end
    rGhostDocument
  end
end
